defmodule Okoshkinet.Repo.Migrations.CreateObjects do
  use Ecto.Migration

  def change do
    create table(:objects) do
      add :street, :string
      add :lat, :float
      add :lng, :float
      add :room_count, :integer
      add :status, :boolean, default: false, null: false
      add :price, :integer
      add :in_dollars, :boolean, default: false, null: false
      add :description, :text

      timestamps()
    end

  end
end
