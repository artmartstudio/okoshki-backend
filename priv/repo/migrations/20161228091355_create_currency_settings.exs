defmodule Okoshkinet.Repo.Migrations.CreateCurrencySettings do
  use Ecto.Migration

  def change do
    create table(:currency_settings) do
      add :day, :integer
      add :price, :integer

      timestamps()
    end

  end
end
