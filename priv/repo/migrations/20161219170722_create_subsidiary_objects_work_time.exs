defmodule Okoshkinet.Repo.Migrations.CreateSubsidiaryObjectsWorkTime do
  use Ecto.Migration

  def change do
    create table(:subsidiary_objects_work_time) do
      add :days_range, :string
      add :time, :string

      timestamps()
    end

  end
end
