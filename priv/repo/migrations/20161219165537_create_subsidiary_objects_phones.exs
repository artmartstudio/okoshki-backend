defmodule Okoshkinet.Repo.Migrations.CreateSubsidiaryObjectsPhones do
  use Ecto.Migration

  def change do
    create table(:subsidiary_objects_phones) do
      add :phone, :string

      timestamps()
    end

  end
end
