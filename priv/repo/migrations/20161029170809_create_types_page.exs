defmodule Okoshkinet.Repo.Migrations.CreateTypesPage do
  use Ecto.Migration

  def change do
    create table(:types_page) do
      add :title, :string
      add :description, :string
      add :mainplace_id, :integer

      timestamps()
    end

  end
end
