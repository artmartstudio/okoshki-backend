defmodule Okoshkinet.Repo.Migrations.CreateHome do
  use Ecto.Migration

  def change do
    create table(:home) do
      add :title, :string
      add :address, :string
      add :phone1, :string
      add :phone2, :string
      add :background, :string

      timestamps()
    end

  end
end
