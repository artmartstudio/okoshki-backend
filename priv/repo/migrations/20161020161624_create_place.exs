defmodule Okoshkinet.Repo.Migrations.CreatePlace do
  use Ecto.Migration

  def change do
    create table(:place) do
      add :title, :string

      timestamps()
    end

  end
end
