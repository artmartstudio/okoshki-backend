defmodule Okoshkinet.Repo.Migrations.CreateSettings do
  use Ecto.Migration

  def change do
    create table(:settings) do
      add :pay, :boolean, default: false, null: false
      add :site_name, :string

      timestamps()
    end

  end
end
