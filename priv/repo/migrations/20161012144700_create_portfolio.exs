defmodule Okoshkinet.Repo.Migrations.CreatePortfolio do
  use Ecto.Migration

  def change do
    create table(:portfolio) do
      add :title, :string
      add :type, :string
      add :marks, :string
      add :url, :string
      add :photo, :string
    end

  end
end
