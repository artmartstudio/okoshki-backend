defmodule Okoshkinet.Repo.Migrations.CreateSubsidiaryObjects2 do
  use Ecto.Migration

  def change do
    create table(:subsidiary_objects) do
      add :title, :string
      add :lat, :float
      add :lng, :float
      add :photo, :string
      add :link, :string
      add :description, :text

      timestamps()
    end

  end
end
