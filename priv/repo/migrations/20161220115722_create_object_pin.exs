defmodule Okoshkinet.Repo.Migrations.CreateObjectPin do
  use Ecto.Migration

  def change do
    create table(:object_pin) do
      add :active, :string
      add :disable, :string

      timestamps()
    end

  end
end
