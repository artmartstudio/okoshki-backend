defmodule Okoshkinet.Repo.Migrations.CreateBlogSettings do
  use Ecto.Migration

  def change do
    create table(:blog_settings) do
      add :title, :string

      timestamps()
    end

  end
end
