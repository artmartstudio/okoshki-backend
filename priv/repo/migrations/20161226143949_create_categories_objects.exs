defmodule Okoshkinet.Repo.Migrations.CreateCategoriesObjects do
  use Ecto.Migration

  def change do
    create table(:categories_objects) do
      add :title, :string

      timestamps()
    end

  end
end
