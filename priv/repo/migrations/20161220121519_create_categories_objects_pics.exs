defmodule Okoshkinet.Repo.Migrations.CreateCategoriesObjectsPics do
  use Ecto.Migration

  def change do
    create table(:categories_objects_pics) do
      add :active, :string
      add :disable, :string
      add :left_panel_icon, :string

      timestamps()
    end

  end
end
