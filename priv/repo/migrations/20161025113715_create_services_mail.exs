defmodule Okoshkinet.Repo.Migrations.CreateServicesMail do
  use Ecto.Migration

  def change do
    create table(:services_mail) do
      add :title1, :string
      add :text_block, :string
      add :title2, :string
      add :title3, :string

      timestamps()
    end

  end
end
