defmodule Okoshkinet.Repo.Migrations.CreateObjectContacts do
  use Ecto.Migration

  def change do
    create table(:object_contacts) do
      add :phone, :string

      timestamps()
    end

  end
end
