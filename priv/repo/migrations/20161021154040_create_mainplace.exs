defmodule Okoshkinet.Repo.Migrations.CreateMainplace do
  use Ecto.Migration

  def change do
    create table(:mainplace) do
      add :title, :string

      timestamps()
    end

  end
end
