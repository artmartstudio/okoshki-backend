defmodule Okoshkinet.Repo.Migrations.CreateCooperation do
  use Ecto.Migration

  def change do
    create table(:cooperation) do
      add :title, :string
      add :description, :text
      add :photo, :string

      timestamps()
    end

  end
end
