defmodule Okoshkinet.Repo.Migrations.CreateBusinessCategory do
  use Ecto.Migration

  def change do
    create table(:business_category) do
      add :types, :string
      add :prefix, :string
      add :title, :string
      add :title_slug, :string
      add :title_declension, :string
      add :marks, :string

      timestamps()
    end

  end
end
