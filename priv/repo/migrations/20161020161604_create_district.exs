defmodule Okoshkinet.Repo.Migrations.CreateDistrict do
  use Ecto.Migration

  def change do
    create table(:district) do
      add :title, :string
      add :place_id, :integer

      timestamps()
    end

  end
end
