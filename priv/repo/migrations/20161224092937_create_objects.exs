defmodule Okoshkinet.Repo.Migrations.CreateObjects do
  use Ecto.Migration

  def change do
    create table(:objects) do
      add :street, :string
      add :lat, :string
      add :lng, :string
      add :room_count, :integer
      add :status_string, :string
      add :price, :integer
      add :in_dollars, :boolean, default: false, null: false
      add :description, :string
      add :photo1, :string
      add :photo2, :string
      add :photo3, :string

      timestamps()
    end

  end
end
