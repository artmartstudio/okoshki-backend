defmodule Okoshkinet.Repo.Migrations.CreateRegion do
  use Ecto.Migration

  def change do
    create table(:region) do
      add :title, :string
      add :district_id, :integer

      timestamps()
    end

  end
end
