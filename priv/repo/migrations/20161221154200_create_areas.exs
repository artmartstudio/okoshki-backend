defmodule Okoshkinet.Repo.Migrations.CreateAreas do
  use Ecto.Migration

  def change do
    create table(:areas) do
      add :title, :string

      timestamps()
    end

  end
end
