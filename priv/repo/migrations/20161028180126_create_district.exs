defmodule Okoshkinet.Repo.Migrations.CreateDistrict do
  use Ecto.Migration

  def change do
    create table(:district) do
      add :title, :string
      add :title_ru, :string

      timestamps()
    end

  end
end
