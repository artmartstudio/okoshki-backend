defmodule Okoshkinet.ObjectsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Objects

  @valid_attrs %{description: "some content", in_dollars: true, lat: "120.5", lng: "120.5", price: 42, room_count: 42, status: true, street: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Objects.changeset(%Objects{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Objects.changeset(%Objects{}, @invalid_attrs)
    refute changeset.valid?
  end
end
