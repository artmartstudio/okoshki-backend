defmodule Okoshkinet.GroupSubsidiaryObjectsWorkTimeTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.GroupSubsidiaryObjectsWorkTime

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GroupSubsidiaryObjectsWorkTime.changeset(%GroupSubsidiaryObjectsWorkTime{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GroupSubsidiaryObjectsWorkTime.changeset(%GroupSubsidiaryObjectsWorkTime{}, @invalid_attrs)
    refute changeset.valid?
  end
end
