defmodule Okoshkinet.SubsidiaryObjects2Test do
  use Okoshkinet.ModelCase

  alias Okoshkinet.SubsidiaryObjects2

  @valid_attrs %{description: "some content", lat: "120.5", link: "some content", lng: "120.5", photo: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SubsidiaryObjects2.changeset(%SubsidiaryObjects2{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SubsidiaryObjects2.changeset(%SubsidiaryObjects2{}, @invalid_attrs)
    refute changeset.valid?
  end
end
