defmodule Okoshkinet.RegionTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Region

  @valid_attrs %{district_id: 42, title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Region.changeset(%Region{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Region.changeset(%Region{}, @invalid_attrs)
    refute changeset.valid?
  end
end
