defmodule Okoshkinet.SettingsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Settings

  @valid_attrs %{pay: true, site_name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Settings.changeset(%Settings{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Settings.changeset(%Settings{}, @invalid_attrs)
    refute changeset.valid?
  end
end
