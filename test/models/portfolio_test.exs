defmodule Okoshkinet.PortfolioTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Portfolio

  @valid_attrs %{marks: "some content", photo: "some content", title: "some content", type: "some content", url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Portfolio.changeset(%Portfolio{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Portfolio.changeset(%Portfolio{}, @invalid_attrs)
    refute changeset.valid?
  end
end
