defmodule Okoshkinet.SubsidiaryObjectsWorkTimeTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.SubsidiaryObjectsWorkTime

  @valid_attrs %{days_range: "some content", time: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SubsidiaryObjectsWorkTime.changeset(%SubsidiaryObjectsWorkTime{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SubsidiaryObjectsWorkTime.changeset(%SubsidiaryObjectsWorkTime{}, @invalid_attrs)
    refute changeset.valid?
  end
end
