defmodule Okoshkinet.SubsidiaryObjectsPhonesTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.SubsidiaryObjectsPhones

  @valid_attrs %{phone: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = SubsidiaryObjectsPhones.changeset(%SubsidiaryObjectsPhones{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = SubsidiaryObjectsPhones.changeset(%SubsidiaryObjectsPhones{}, @invalid_attrs)
    refute changeset.valid?
  end
end
