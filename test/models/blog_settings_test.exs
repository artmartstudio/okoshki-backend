defmodule Okoshkinet.BlogSettingsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.BlogSettings

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = BlogSettings.changeset(%BlogSettings{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = BlogSettings.changeset(%BlogSettings{}, @invalid_attrs)
    refute changeset.valid?
  end
end
