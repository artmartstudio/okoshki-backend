defmodule Okoshkinet.CategoriesObjectsPicsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.CategoriesObjectsPics

  @valid_attrs %{active: "some content", disable: "some content", left_panel_icon: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = CategoriesObjectsPics.changeset(%CategoriesObjectsPics{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = CategoriesObjectsPics.changeset(%CategoriesObjectsPics{}, @invalid_attrs)
    refute changeset.valid?
  end
end
