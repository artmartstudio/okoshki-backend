defmodule Okoshkinet.MenuTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Menu

  @valid_attrs %{sort: 42, title: "some content", url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Menu.changeset(%Menu{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Menu.changeset(%Menu{}, @invalid_attrs)
    refute changeset.valid?
  end
end
