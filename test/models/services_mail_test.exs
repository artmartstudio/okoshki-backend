defmodule Okoshkinet.ServicesMailTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.ServicesMail

  @valid_attrs %{text_block: "some content", title1: "some content", title2: "some content", title3: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ServicesMail.changeset(%ServicesMail{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ServicesMail.changeset(%ServicesMail{}, @invalid_attrs)
    refute changeset.valid?
  end
end
