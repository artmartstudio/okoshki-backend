defmodule Okoshkinet.AreasTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Areas

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Areas.changeset(%Areas{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Areas.changeset(%Areas{}, @invalid_attrs)
    refute changeset.valid?
  end
end
