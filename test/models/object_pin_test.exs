defmodule Okoshkinet.ObjectPinTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.ObjectPin

  @valid_attrs %{active: "some content", disable: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ObjectPin.changeset(%ObjectPin{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ObjectPin.changeset(%ObjectPin{}, @invalid_attrs)
    refute changeset.valid?
  end
end
