defmodule Okoshkinet.GroupSubsidiaryObjectsPhonesTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.GroupSubsidiaryObjectsPhones

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GroupSubsidiaryObjectsPhones.changeset(%GroupSubsidiaryObjectsPhones{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GroupSubsidiaryObjectsPhones.changeset(%GroupSubsidiaryObjectsPhones{}, @invalid_attrs)
    refute changeset.valid?
  end
end
