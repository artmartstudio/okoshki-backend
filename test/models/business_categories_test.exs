defmodule Okoshkinet.BusinessCategoriesTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.BusinessCategories

  @valid_attrs %{alt: "some content", icon: "some content", title: "some content", type_id: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = BusinessCategories.changeset(%BusinessCategories{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = BusinessCategories.changeset(%BusinessCategories{}, @invalid_attrs)
    refute changeset.valid?
  end
end
