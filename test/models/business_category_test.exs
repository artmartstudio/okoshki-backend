defmodule Okoshkinet.BusinessCategoryTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.BusinessCategory

  @valid_attrs %{marks: "some content", prefix: "some content", title: "some content", title_declension: "some content", title_slug: "some content", types: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = BusinessCategory.changeset(%BusinessCategory{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = BusinessCategory.changeset(%BusinessCategory{}, @invalid_attrs)
    refute changeset.valid?
  end
end
