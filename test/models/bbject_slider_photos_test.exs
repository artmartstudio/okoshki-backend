defmodule Okoshkinet.BbjectSliderPhotosTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.BbjectSliderPhotos

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = BbjectSliderPhotos.changeset(%BbjectSliderPhotos{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = BbjectSliderPhotos.changeset(%BbjectSliderPhotos{}, @invalid_attrs)
    refute changeset.valid?
  end
end
