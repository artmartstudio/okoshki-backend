defmodule Okoshkinet.HomeTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Home

  @valid_attrs %{address: "some content", background: "some content", phone1: "some content", phone2: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Home.changeset(%Home{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Home.changeset(%Home{}, @invalid_attrs)
    refute changeset.valid?
  end
end
