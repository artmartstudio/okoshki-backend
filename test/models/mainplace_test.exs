defmodule Okoshkinet.MainplaceTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Mainplace

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Mainplace.changeset(%Mainplace{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Mainplace.changeset(%Mainplace{}, @invalid_attrs)
    refute changeset.valid?
  end
end
