defmodule Okoshkinet.SitetypeTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Sitetype

  @valid_attrs %{icon: "some content", text: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Sitetype.changeset(%Sitetype{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Sitetype.changeset(%Sitetype{}, @invalid_attrs)
    refute changeset.valid?
  end
end
