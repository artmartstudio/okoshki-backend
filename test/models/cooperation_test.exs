defmodule Okoshkinet.CooperationTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Cooperation

  @valid_attrs %{description: "some content", photo: "some content", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Cooperation.changeset(%Cooperation{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Cooperation.changeset(%Cooperation{}, @invalid_attrs)
    refute changeset.valid?
  end
end
