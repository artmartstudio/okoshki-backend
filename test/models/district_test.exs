defmodule Okoshkinet.DistrictTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.District

  @valid_attrs %{place_id: 42, title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = District.changeset(%District{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = District.changeset(%District{}, @invalid_attrs)
    refute changeset.valid?
  end
end
