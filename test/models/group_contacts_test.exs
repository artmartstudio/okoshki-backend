defmodule Okoshkinet.GroupContactsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.GroupContacts

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GroupContacts.changeset(%GroupContacts{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GroupContacts.changeset(%GroupContacts{}, @invalid_attrs)
    refute changeset.valid?
  end
end
