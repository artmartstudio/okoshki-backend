defmodule Okoshkinet.CategoriesTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.Categories

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Categories.changeset(%Categories{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Categories.changeset(%Categories{}, @invalid_attrs)
    refute changeset.valid?
  end
end
