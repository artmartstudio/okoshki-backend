defmodule Okoshkinet.CategoriesObjectsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.CategoriesObjects

  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = CategoriesObjects.changeset(%CategoriesObjects{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = CategoriesObjects.changeset(%CategoriesObjects{}, @invalid_attrs)
    refute changeset.valid?
  end
end
