defmodule Okoshkinet.GroupObjectSliderPhotosTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.GroupObjectSliderPhotos

  @valid_attrs %{}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GroupObjectSliderPhotos.changeset(%GroupObjectSliderPhotos{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GroupObjectSliderPhotos.changeset(%GroupObjectSliderPhotos{}, @invalid_attrs)
    refute changeset.valid?
  end
end
