defmodule Okoshkinet.TypesPageTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.TypesPage

  @valid_attrs %{description: "some content", mainplace_id: 42, title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = TypesPage.changeset(%TypesPage{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = TypesPage.changeset(%TypesPage{}, @invalid_attrs)
    refute changeset.valid?
  end
end
