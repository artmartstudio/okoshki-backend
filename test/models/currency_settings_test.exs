defmodule Okoshkinet.CurrencySettingsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.CurrencySettings

  @valid_attrs %{day: 42, price: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = CurrencySettings.changeset(%CurrencySettings{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = CurrencySettings.changeset(%CurrencySettings{}, @invalid_attrs)
    refute changeset.valid?
  end
end
