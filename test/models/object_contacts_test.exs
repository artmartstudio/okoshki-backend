defmodule Okoshkinet.ObjectContactsTest do
  use Okoshkinet.ModelCase

  alias Okoshkinet.ObjectContacts

  @valid_attrs %{phone: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ObjectContacts.changeset(%ObjectContacts{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ObjectContacts.changeset(%ObjectContacts{}, @invalid_attrs)
    refute changeset.valid?
  end
end
