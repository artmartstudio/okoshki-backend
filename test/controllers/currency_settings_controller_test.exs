defmodule Okoshkinet.CurrencySettingsControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.CurrencySettings
  @valid_attrs %{day: 42, price: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, currency_settings_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    currency_settings = Repo.insert! %CurrencySettings{}
    conn = get conn, currency_settings_path(conn, :show, currency_settings)
    assert json_response(conn, 200)["data"] == %{"id" => currency_settings.id,
      "day" => currency_settings.day,
      "price" => currency_settings.price}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, currency_settings_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, currency_settings_path(conn, :create), currency_settings: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(CurrencySettings, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, currency_settings_path(conn, :create), currency_settings: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    currency_settings = Repo.insert! %CurrencySettings{}
    conn = put conn, currency_settings_path(conn, :update, currency_settings), currency_settings: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(CurrencySettings, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    currency_settings = Repo.insert! %CurrencySettings{}
    conn = put conn, currency_settings_path(conn, :update, currency_settings), currency_settings: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    currency_settings = Repo.insert! %CurrencySettings{}
    conn = delete conn, currency_settings_path(conn, :delete, currency_settings)
    assert response(conn, 204)
    refute Repo.get(CurrencySettings, currency_settings.id)
  end
end
