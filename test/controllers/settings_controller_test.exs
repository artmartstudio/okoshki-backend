defmodule Okoshkinet.SettingsControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.Settings
  @valid_attrs %{pay: true, site_name: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, settings_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    settings = Repo.insert! %Settings{}
    conn = get conn, settings_path(conn, :show, settings)
    assert json_response(conn, 200)["data"] == %{"id" => settings.id,
      "pay" => settings.pay,
      "site_name" => settings.site_name}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, settings_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, settings_path(conn, :create), settings: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Settings, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, settings_path(conn, :create), settings: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    settings = Repo.insert! %Settings{}
    conn = put conn, settings_path(conn, :update, settings), settings: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Settings, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    settings = Repo.insert! %Settings{}
    conn = put conn, settings_path(conn, :update, settings), settings: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    settings = Repo.insert! %Settings{}
    conn = delete conn, settings_path(conn, :delete, settings)
    assert response(conn, 204)
    refute Repo.get(Settings, settings.id)
  end
end
