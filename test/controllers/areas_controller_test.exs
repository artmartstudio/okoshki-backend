defmodule Okoshkinet.AreasControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.Areas
  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, areas_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    areas = Repo.insert! %Areas{}
    conn = get conn, areas_path(conn, :show, areas)
    assert json_response(conn, 200)["data"] == %{"id" => areas.id,
      "title" => areas.title}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, areas_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, areas_path(conn, :create), areas: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Areas, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, areas_path(conn, :create), areas: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    areas = Repo.insert! %Areas{}
    conn = put conn, areas_path(conn, :update, areas), areas: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Areas, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    areas = Repo.insert! %Areas{}
    conn = put conn, areas_path(conn, :update, areas), areas: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    areas = Repo.insert! %Areas{}
    conn = delete conn, areas_path(conn, :delete, areas)
    assert response(conn, 204)
    refute Repo.get(Areas, areas.id)
  end
end
