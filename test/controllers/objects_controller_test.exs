defmodule Okoshkinet.ObjectsControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.Objects
  @valid_attrs %{description: "some content", in_dollars: true, lat: "some content", lng: "some content", photo1: "some content", photo2: "some content", photo3: "some content", price: 42, room_count: 42, status_string: "some content", street: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, objects_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    objects = Repo.insert! %Objects{}
    conn = get conn, objects_path(conn, :show, objects)
    assert json_response(conn, 200)["data"] == %{"id" => objects.id,
      "street" => objects.street,
      "lat" => objects.lat,
      "lng" => objects.lng,
      "room_count" => objects.room_count,
      "status_string" => objects.status_string,
      "price" => objects.price,
      "in_dollars" => objects.in_dollars,
      "description" => objects.description,
      "photo1" => objects.photo1,
      "photo2" => objects.photo2,
      "photo3" => objects.photo3}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, objects_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, objects_path(conn, :create), objects: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Objects, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, objects_path(conn, :create), objects: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    objects = Repo.insert! %Objects{}
    conn = put conn, objects_path(conn, :update, objects), objects: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Objects, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    objects = Repo.insert! %Objects{}
    conn = put conn, objects_path(conn, :update, objects), objects: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    objects = Repo.insert! %Objects{}
    conn = delete conn, objects_path(conn, :delete, objects)
    assert response(conn, 204)
    refute Repo.get(Objects, objects.id)
  end
end
