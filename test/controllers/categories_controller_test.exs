defmodule Okoshkinet.CategoriesControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.Categories
  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, categories_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    categories = Repo.insert! %Categories{}
    conn = get conn, categories_path(conn, :show, categories)
    assert json_response(conn, 200)["data"] == %{"id" => categories.id,
      "title" => categories.title}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, categories_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, categories_path(conn, :create), categories: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Categories, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, categories_path(conn, :create), categories: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    categories = Repo.insert! %Categories{}
    conn = put conn, categories_path(conn, :update, categories), categories: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Categories, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    categories = Repo.insert! %Categories{}
    conn = put conn, categories_path(conn, :update, categories), categories: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    categories = Repo.insert! %Categories{}
    conn = delete conn, categories_path(conn, :delete, categories)
    assert response(conn, 204)
    refute Repo.get(Categories, categories.id)
  end
end
