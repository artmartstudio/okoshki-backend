defmodule Okoshkinet.BusinessCategoryControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.BusinessCategory
  @valid_attrs %{marks: "some content", prefix: "some content", title: "some content", title_declension: "some content", title_slug: "some content", types: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, business_category_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    business_category = Repo.insert! %BusinessCategory{}
    conn = get conn, business_category_path(conn, :show, business_category)
    assert json_response(conn, 200)["data"] == %{"id" => business_category.id,
      "types" => business_category.types,
      "prefix" => business_category.prefix,
      "title" => business_category.title,
      "title_slug" => business_category.title_slug,
      "title_declension" => business_category.title_declension,
      "marks" => business_category.marks}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, business_category_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, business_category_path(conn, :create), business_category: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(BusinessCategory, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, business_category_path(conn, :create), business_category: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    business_category = Repo.insert! %BusinessCategory{}
    conn = put conn, business_category_path(conn, :update, business_category), business_category: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(BusinessCategory, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    business_category = Repo.insert! %BusinessCategory{}
    conn = put conn, business_category_path(conn, :update, business_category), business_category: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    business_category = Repo.insert! %BusinessCategory{}
    conn = delete conn, business_category_path(conn, :delete, business_category)
    assert response(conn, 204)
    refute Repo.get(BusinessCategory, business_category.id)
  end
end
