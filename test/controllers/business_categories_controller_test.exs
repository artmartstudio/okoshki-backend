defmodule Okoshkinet.BusinessCategoriesControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.BusinessCategories
  @valid_attrs %{alt: "some content", icon: "some content", title: "some content", type_id: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, business_categories_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    business_categories = Repo.insert! %BusinessCategories{}
    conn = get conn, business_categories_path(conn, :show, business_categories)
    assert json_response(conn, 200)["data"] == %{"id" => business_categories.id,
      "title" => business_categories.title,
      "icon" => business_categories.icon,
      "alt" => business_categories.alt,
      "type_id" => business_categories.type_id}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, business_categories_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, business_categories_path(conn, :create), business_categories: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(BusinessCategories, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, business_categories_path(conn, :create), business_categories: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    business_categories = Repo.insert! %BusinessCategories{}
    conn = put conn, business_categories_path(conn, :update, business_categories), business_categories: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(BusinessCategories, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    business_categories = Repo.insert! %BusinessCategories{}
    conn = put conn, business_categories_path(conn, :update, business_categories), business_categories: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    business_categories = Repo.insert! %BusinessCategories{}
    conn = delete conn, business_categories_path(conn, :delete, business_categories)
    assert response(conn, 204)
    refute Repo.get(BusinessCategories, business_categories.id)
  end
end
