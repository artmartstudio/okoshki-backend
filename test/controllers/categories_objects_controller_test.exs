defmodule Okoshkinet.CategoriesObjectsControllerTest do
  use Okoshkinet.ConnCase

  alias Okoshkinet.CategoriesObjects
  @valid_attrs %{title: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, categories_objects_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    categories_objects = Repo.insert! %CategoriesObjects{}
    conn = get conn, categories_objects_path(conn, :show, categories_objects)
    assert json_response(conn, 200)["data"] == %{"id" => categories_objects.id,
      "title" => categories_objects.title}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, categories_objects_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, categories_objects_path(conn, :create), categories_objects: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(CategoriesObjects, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, categories_objects_path(conn, :create), categories_objects: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    categories_objects = Repo.insert! %CategoriesObjects{}
    conn = put conn, categories_objects_path(conn, :update, categories_objects), categories_objects: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(CategoriesObjects, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    categories_objects = Repo.insert! %CategoriesObjects{}
    conn = put conn, categories_objects_path(conn, :update, categories_objects), categories_objects: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    categories_objects = Repo.insert! %CategoriesObjects{}
    conn = delete conn, categories_objects_path(conn, :delete, categories_objects)
    assert response(conn, 204)
    refute Repo.get(CategoriesObjects, categories_objects.id)
  end
end
