# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :okoshkinet,
  ecto_repos: [Okoshkinet.Repo]

config :slugger, separator_char: ?-

config :ex_admin,
  title: "Okoshkinet | Admin",
  logo_mini: "ON",
  logo_full: "OkoshkinetAdmin",
  repo: Okoshkinet.Repo,
  module: Okoshkinet,
  modules: [
    Okoshkinet.ExAdmin.Dashboard,
    Okoshkinet.ExAdmin.Categories,
    Okoshkinet.ExAdmin.CategoriesObjects,
    Okoshkinet.ExAdmin.SubsidiaryObjects,
    Okoshkinet.ExAdmin.Objects,
    Okoshkinet.ExAdmin.Areas,
    Okoshkinet.ExAdmin.ObjectPin,
    Okoshkinet.ExAdmin.CategoriesObjectsPics,
    Okoshkinet.ExAdmin.Settings,
    Okoshkinet.ExAdmin.CurrencySettings,
  ]

config :okoshkinet, Okoshkinet.Mailer,
  adapter: Bamboo.SMTPAdapter,
  server: "smtp.gmail.com",
  port: 587,
  username: "artmartmailer@gmail.com",
  password: "artmartmanager",
  tls: :always, # can be `:always` or `:never`
  ssl: false, # can be `true`
  retries: 1

config :quantum, :okoshkinet,
  cron: [
    "@daily": {Okoshkinet.ApiController, :update_objects}
  ]

# Configures the endpoint
config :okoshkinet, Okoshkinet.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ZeZnNKlutUTadeu8qEvOmqF2mD+wRaYF9nGeokY2cUZ6UrP38z6JDeZDKSEbB82T",
  render_errors: [view: Okoshkinet.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Okoshkinet.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :xain, :after_callback, {Phoenix.HTML, :raw}
