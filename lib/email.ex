defmodule Okoshkinet.Email do
  use Bamboo.Phoenix, view: Okoshkinet.EmailView

  def send(email_address, subject, text_body) do
    new_email
    |> to(email_address)
    |> from("artmartmailer@gmail.com")
    |> subject(subject)
    |> text_body(text_body)
  end
end
