defmodule Okoshkinet.Repo do
  use Ecto.Repo, otp_app: :okoshkinet
  use Scrivener, page_size: 10
end
