import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { Provider } from 'react-redux'
import { match, RouterContext } from 'react-router'
import createMemoryHistory from 'history/lib/createMemoryHistory'
import Helmet from 'react-helmet'
import { getStore, createRoutes, initialState } from 'routes'
import template from './templates/index.tmpl.html'
import axios from 'axios'

class Server {
  static start(port) {
    return this.startServer(port)
  }

  static renderFullPage(html, initialState, helmetMeta, title) {
    return template({
      html,
      initialState: JSON.stringify(initialState),
      helmetMeta,
      title
    })
  }

  static checkMethodExists(Component, method) {
    while (Component.DecoratedComponent) {
      Component = Component.DecoratedComponent
    }
    return typeof Component[method] !== 'undefined'
  }

  static callMethod(Component, method, ...args) {
    while (Component.DecoratedComponent) {
      Component = Component.DecoratedComponent
    }
    return Component[method] && Component[method](...args)
  }

  static arrayToObject(array) {
    var obj = {}
    array.forEach((el) => {
      Object.assign(obj, el)
    })
    return obj
  }

  static componentToHtml(store, props, initialData) {
    var createElementFn = serverProps => (Component, props) => (<Component {...serverProps} {...props} />)
    var html = ReactDOMServer.renderToString(
      <Provider store={store}>
        <RouterContext {...props} createElement={createElementFn({initialData})} />
      </Provider>
    )
    var head = Helmet.rewind()
    var meta = head.meta.toString()
    var title = head.title.toString()
    return { html, meta, title }
  }

  static reactRender(req, res, routes) {
    match({routes, location: req.url}, (error, redirect, props) => {
      if (error) {
        res.status(500).send(error.message)
      } else if (redirect) {
        res.redirect(302, redirect.pathname + redirect.search)
      } else if (props) {
        var store = getStore(initialState)
        var comp = props.routes[props.routes.length - 1].component
        if (comp) {
          if (this.checkMethodExists(comp, 'initialData')) {
            this.callMethod(comp, 'initialData', props).then(initialData => {
              if (Array.isArray(initialData)) {
                initialData = this.arrayToObject(initialData)
              }
              var { html, meta, title } = this.componentToHtml(store, props, initialData)
              res.status(200).send(this.renderFullPage(html, Object.assign(initialState, initialData), meta, title))
            })
          } else {
            var { html, meta, title } = this.componentToHtml(store, props, null)
            res.status(200).send(this.renderFullPage(html, initialState, meta, title))
          }
        }
      } else {
        res.status(404).send('Not found')
      }
    })
  }

  static startServer(port) {
    var app = express()
    var history = createMemoryHistory()
    var routes = createRoutes(history)
    app.use(cookieParser())
    app.use(bodyParser.json())
    app.use('/static', express.static('static'))
    global.navigator = { userAgent: 'all' }
    // react js seo
    app.get('*', (req, res) => {
      axios.defaults.baseURL = `${req.protocol}://${req.get('Host')}`
      this.reactRender(req, res, routes)
    })
    app.listen(port)
  }
}
Server.start(process.env.PORT || 9000)
