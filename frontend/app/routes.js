import React from 'react'
import thunk from 'redux-thunk'
import { Router, Route, IndexRoute } from 'react-router'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import App from './App.jsx'
import IndexPage from 'views/IndexPage/Index.jsx'
import PageNotFound from 'views/PageNotFound/Index.jsx'

import mapReducer from 'reducers/map'
import objectsReducer from 'reducers/objects'
import categoriesReducer from 'reducers/categories'
import directionReducer from 'reducers/direction'
import authReducer from 'reducers/auth'
import payReducer from 'reducers/pay'

var initialState = {
  map: {
    mapObject: undefined,
  },

  objects: {
    objectsList: [],
  },

  categories: {
    opened: true,
  },

  direction: {
    opened: false,
    lat: 0,
    lng: 0,
    reverse: false,
    to: ''
  },

  auth: {
    isAuth: false,
    userRole: undefined,
    expDays: 0,
    popupAuthOpen: false,
    popupRegistrationOpen: false,
    popupForgotPasswordOpen: false,
    registrationPay: false,
  },

  pay: {
    days: [],
    currentIndex: undefined,
    data: undefined,
    signature: undefined,
    showPopup: false,
    isPay: false,
  }
}

export {initialState}

export function getStore(state = initialState) {
  return createStore(
    combineReducers({
      routing: routerReducer,
      map: mapReducer,
      objects: objectsReducer,
      categories: categoriesReducer,
      direction: directionReducer,
      auth: authReducer,
      pay: payReducer,
    }),
    state,
    applyMiddleware(thunk)
  )
}

export function getInitialState() {
  return window.__INITIAL_STATE__
}

export function createRoutes(history, store) {
  if (store) {
    history = syncHistoryWithStore(history, store)
  }

  var onUpdate = () => {
    window.scrollTo(0, 0)
  }

  return (
    <Router history={history} onUpdate={onUpdate}>
      <Route component={App}>
        <Route path='*' component={IndexPage} />
      </Route>
    </Router>
  )
}
