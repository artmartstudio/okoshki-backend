export function toggleCategories(opened) {
  return (dispatch, getState) => {
    dispatch({
      type: "TOGGLE_CATEGORIES",
      opened
    })
  }
}
