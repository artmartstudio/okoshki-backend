export function ymapSetObject(key, value) {
  return (dispatch, getState) => {
    dispatch({type: "YMAP_SET_OBJECT", key, value})
  }
}
