export function directionSet(key, value) {
  return (dispatch, getState) => {
    dispatch({type: "DIRECTION_SET_OBJECT", key, value})
  }
}
