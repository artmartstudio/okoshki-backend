export function objectsSet(key, value) {
  return (dispatch, getState) => {
    dispatch({type: "OBJECTS_SET", key, value})
  }
}
