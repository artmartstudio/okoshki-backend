export function authSet(key, value) {
  return (dispatch, getState) => {
    dispatch({type: "AUTH_SET_OBJECT", key, value})
  }
}
