import { assign } from 'lodash/object'

export default (state = {}, action) => {
  switch (action.type) {
    case "OBJECTS_SET":
      return assign({}, state, {[action.key]: action.value})
    default:
      return state
  }
}
