import { assign } from 'lodash/object'

export default (state = {}, action) => {
  var newState = assign({}, state)

  switch (action.type) {
    case "TOGGLE_CATEGORIES":
      var opened = action.opened != undefined
        ? action.opened
        : !newState.opened
      newState.opened = opened
      return newState

    default:
      return state
  }
}
