import React from 'react'

export default class PageNotFound extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <h1>Page not found</h1>
      </div>
    )
  }
}
