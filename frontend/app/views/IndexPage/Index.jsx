import React from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import axios from 'axios'
import { authSet } from 'actions/auth'
import { isNull } from 'lodash/lang'

import Category from 'components/Category'
import Header from 'components/Header'
import Objects from 'components/Objects'

class IndexPage extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    var token = localStorage.getItem('token')

    if (!isNull(token)) {
      axios.post('/api/isauth', {token: token}).then((resp) => {
        if (resp.data.status === 'authenticated') {
          axios.post('/api/user.get', {token: token}).then((resp) => {
            this.props.dispatch(authSet('userRole', resp.data))
          })
          this.props.dispatch(authSet('isAuth', true))
        }
      })
    }
  }

  render() {
    return (
      <div>
        <Header auth={this.props.auth} />
        {this.props.map && <Category />}
        {this.props.objects.length > 0 && <Objects />}
      </div>
    )
  }
}

export default connect(
  state => ({
    objects: state.objects.objectsList,
    map: state.map.mapObject,
    auth: state.auth,
  })
)(IndexPage)
