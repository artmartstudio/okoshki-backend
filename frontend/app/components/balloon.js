import ReactDOM from 'react-dom/server'
import { toggleCategories } from 'actions/categories'
import { directionSet } from 'actions/direction'
import React from 'react'
import BalloonContent from 'components/BalloonContent'

export default (object, map) => {
  var balloonContent = ReactDOM.renderToString(<BalloonContent object={object} />)

  return map.balloon.open([object.lat, object.lng],
    {content: balloonContent},
    { closeButton: true,
      offset: [0, -32],
      minWidth: 410,
      mixHeight: 440,
      maxHeight: 800,
      shadow: false })
}
