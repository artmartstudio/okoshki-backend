import React from 'react'
import $ from 'jquery'

export default class EventListener extends React.Component {
  constructor(props) {
    super(props)

    this.target = this.props.target
    this.eventList = []
  }

  componentDidMount() {
    if (typeof this.target === 'string' && this.props.type === 'global') {
      this.target = global[this.target]
    }

    if (this.props.onClick !== undefined) {
      $(this.target).on('click', this.props.onClick)
      this.eventList.push('click')
    }

    if (this.props.onResize !== undefined) {
      $(this.target).on('resize', this.props.onResize)
      this.eventList.push('resize')
    }

    if (this.props.onScroll !== undefined) {
      $(this.target).on('scroll', this.props.onScroll)
      this.eventList.push('scroll')
    }

    if (this.props.onKeyDown !== undefined) {
      $(this.target).on('keydown', this.props.onKeyDown)
      this.eventList.push('keydown')
    }

    if (this.props.onMouseWheel !== undefined) {
      this.target.addEventListener('mousewheel', this.props.onMouseWheel, true)
      this.target.addEventListener('DOMMouseScroll', this.props.onMouseWheel, true)
      // $(this.target).on('mousewheel', this.props.onMouseWheel)
      // this.eventList.push('mousewheel')
    }
  }

  componentWillUnmount() {
    this.eventList.map((el) => {
      this.target.removeEventListener('mousewheel', this.props.onMouseWheel, true)
      this.target.removeEventListener('DOMMouseScroll', this.props.onMouseWheel, true)

      if (this.target !== undefined) {
        $(this.target).off(el)
      }      
    })
  }

  render() {
    return null
  }
}