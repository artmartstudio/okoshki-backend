import React from 'react'
import chunk from 'lodash/chunk'
import pluralize from 'common/pluralize'

import 'styles/balloonobj'

import SliderArrow from 'images/slider-arrow.svg'

export default class BalloonObjContent extends React.Component {
  constructor(props) {
    super(props)
  }

  formatPrice(price) {
    return chunk(price.toString().split('.')[0].split('').reverse(), 3)
      .map(e => e.reverse().join(''))
      .reverse()
      .join(' ')
  }

  render() {
    var photos = [this.props.object.photo1,
      this.props.object.photo2,
      this.props.object.photo3]

    photos = photos.map((photo) => {
      if (photo != undefined && photo.file_name.length > 0)
        return photo.file_name
    })

    return (
      <div className="baloon-obj-content">
        <div className="slider">
          <div className="buttons">
            <div className="prev" dangerouslySetInnerHTML={{__html: SliderArrow}}></div>
            <div className="next" dangerouslySetInnerHTML={{__html: SliderArrow}}></div>
          </div>

          <div className="img-list">
            {photos.map((photo, i) => <img key={i} src={`/uploads/object_photos/original_${photo}.jpg`} />)}
          </div>
        </div>

        <h2>{this.props.object.street}</h2>
        <span>{this.props.object.area.title} район</span>

        <div className="hr"></div>

        <div className="info-block">
          <div>{this.props.object.room_count} {pluralize(this.props.object.room_count, ['комната', 'комнаты', 'комнат'])}</div>
          <div>{this.props.object.status}</div>
          <span>{this.props.object.in_dollars && `$`} {!this.props.object.in_dollars && `₴`} {this.formatPrice(this.props.object.price)}</span>
        </div>

        {!this.props.showContacts && (
          <div>
            <p>Чтобы просмотреть контакты, оформите платную подписку:</p>

            <div className="pay-block">
              {this.props.days.map((day, i) => <div key={i}><span>{day.day} {pluralize(day.day, ['день', 'дня', 'дней'])}</span><object>₴ {day.amount}</object></div>)}
            </div>

            <button id="pay" className="disabled">ОПЛАТИТЬ</button>
          </div>
        )}

        {this.props.showContacts && (
          <div className="description-block">
            <p className="description">
              {this.props.object.description}
            </p>

            <div>
              <ul>
                {this.props.object.phones.map((phone, i) => <li key={i}><a href={`skype:${phone.phone}?call`}>{phone.phone}</a></li>)}
              </ul>

              <button className="route" id="route-setter">МАРШРУТ</button>
            </div>
          </div>
        )}
      </div>
    )
  }
}
