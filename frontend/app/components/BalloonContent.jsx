import React from 'react'

import 'styles/balloon'

export default class BalloonContent extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="baloon-content">
        <div className="baloon-header">
          <img src={this.props.object.photo && `/static/uploads/object_photos/original_${this.props.object.photo.file_name}.jpg`} alt="" />
          <h2>{this.props.object.title}</h2>
          <a target="_blank" href={`http://${this.props.object.link}`}>{this.props.object.link}</a>
        </div>

        <p className="baloon-content-p">
          {this.props.object.description}
        </p>

        <div className="baloon-contacts">
          <ul className="numbers">
            {this.props.object.phones.map((phone, idx) => {
              return <li key={idx}><a>{phone.phone}</a></li>
            })}
          </ul>

          <ul className="work-time">
            {this.props.object.work_time.map((workTime, idx) => {
              return <li key={idx}>{workTime.days_range} &#8226; {workTime.time}</li>
            })}
          </ul>
        </div>

        <div className="baloon-footer">
          <button id="route-setter" data-lat={this.props.object.lat} data-lng={this.props.object.lng}>Проложить маршрут</button>
        </div>
      </div>
    )
  }
}
