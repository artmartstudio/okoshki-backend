import React from 'react'
import ReactDOM from 'react-dom/server'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { Scrollbars } from 'react-custom-scrollbars'
import { assign } from 'lodash/object'
import { toggleCategories } from 'actions/categories'
import { directionSet } from 'actions/direction'
import { isNull } from 'lodash/lang'

import BalloonEvent from 'components/balloon'

import 'styles/objects'
import 'styles/map'

import cart from 'images/cart.svg'

class Objects extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      opened: false,
    }
  }

  mapClick(object) {
    this.props.map.setCenter([object.lat, object.lng], 17, {checkZoomRange: false})

    var balloon = BalloonEvent(object, this.props.map)

    document.getElementById('route-setter').addEventListener('click', () => {
      this.props.dispatch(toggleCategories(false))
      balloon.close()
      this.props.dispatch(directionSet('lat', object.lat))
      this.props.dispatch(directionSet('lng', object.lng))
      this.props.dispatch(directionSet('to', object.title))
      this.props.dispatch(directionSet('opened', true))
    })

    this.setState({opened: false})
  }

  render() {
    return (
      <div
        className={classNames("right-panel", {"hidden": !this.state.opened})} >
        <div className="right-panel__header" onClick={() => this.setState({opened: !this.state.opened})}>
          <h2>Объекты</h2>

          <span className="right-panel__objects_count">{this.props.objects.length}</span>
          <span className="right-sidebar__close"></span>
        </div>

        <div className="right-sidebar__content">
          <div className="table">
            {this.props.objects.map((object, key) => {
              return (
                <div className="row" key={key}>
                  <div className="cell"><span dangerouslySetInnerHTML={{__html: cart}}></span>{object.title}</div>
                  <div className="cell"><span className="address">{object.address}</span></div>
                  <div className="cell tablet-only mobile-only"><a href={`http://${object.link}`} target="_blank">{object.link}</a></div>
                  <div className="cell cell-numbers">
                    <ul className="numbers">
                      {object.phones.map((phone, idx) => {
                        return <li key={idx}><a href={`skype:${phone.phone.trim()}?call`}>{phone.phone}</a></li>
                      })}
                    </ul>
                  </div>
                  <div className="cell desktop-only"><a href={`http://${object.link}`} target="_blank">{object.link}</a></div>
                  <div className="cell cell-work-time">
                    <ul className="work-time">
                      {object.work_time.map((workTime, i) => {
                        return <li key={i}>{workTime.days_range} {!isNull(workTime.days_range) > 0 && `&#8226;`} {workTime.time}</li>
                      })}
                    </ul>
                  </div>
                  <div className="cell">
                    <div className="map-btn" onClick={this.mapClick.bind(this, object)} />
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    objects: state.objects.objectsList,
    map: state.map.mapObject,
    mapPopup: state.mapPopup,
  })
)(Objects)
