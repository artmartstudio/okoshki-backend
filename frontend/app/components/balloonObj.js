import ReactDOM from 'react-dom/server'
import { toggleCategories } from 'actions/categories'
import { directionSet } from 'actions/direction'
import React from 'react'
import BalloonObjContent from 'components/BalloonObjContent'
import { isNaN } from 'lodash/lang'

export default (object, map, pay, auth) => {
  var days = pay.days
  var showContacts = !pay.isPay
  if (auth.isAuth) {
    showContacts = (auth.expDays > 0 || isNaN(auth.expDays))
  }

  var balloonContent = ReactDOM.renderToString(<BalloonObjContent
    showContacts={showContacts}
    days={days}
    object={object} />)

  return map.balloon.open([object.lat, object.lng],
    {content: balloonContent},
    { closeButton: true,
      offset: [0, -32],
      minWidth: 410,
      mixHeight: 585,
      maxHeight: 800,
      shadow: false })
}
