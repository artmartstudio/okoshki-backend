import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import axios from 'axios'
import $ from 'jquery'
import { isNull, cloneDeep } from 'lodash/lang'
import ReactDOM from 'react-dom/server'

import { getInitialState } from 'routes'
import { objectsSet } from 'actions/objects'
import { toggleCategories } from 'actions/categories'
import { directionSet } from 'actions/direction'
import { paySet } from 'actions/pay'
import { authSet } from 'actions/auth'
import { Scrollbars } from 'react-custom-scrollbars'
import { connect } from 'react-redux'
import { assign } from 'lodash/object'

import 'styles/object-list'
import 'styles/search-result'

import arrow from 'images/arrow.svg'
import house from 'images/house.svg'

import Checkbox from 'components/Checkbox'
import RadioButtonGroup from 'components/RadioButtonGroup'
import Dropdown from 'components/Dropdown'
import BalloonEvent from 'components/balloon'
import BalloonObjEvent from 'components/balloonObj'

class Category extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      objectActive: undefined,
      objectFirstLvlActive: undefined,
      listActive: undefined,
      areas: undefined,
      categories: undefined,
      searchObjects: undefined,
      fullCategories: undefined,
      objects: undefined,
      objectsFirstLvl: undefined,
      selectObjects: [],
      rent: false,
      sale: true,
      foundObjects: [],
      foundPlacemarks: []
    }

    this.content = [
      {title: '1', value: 1},
      {title: '2', value: 2},
      {title: '3', value: 3},
      {title: '4 и более', value: 'more'},
    ]

    this.placemarks = []
    this.objectsPlacemarks = []

    this.filter = {
      status: undefined,
      room_count: undefined,
      area_id: undefined,
      more: undefined,
    }
  }

  static initialData() {
    var defaultStatus = 'Продается'

    return axios.all([
      axios.get('/api/kiev.areas.get'),
      axios.get('/api/categories.get'),
      axios.get(`/api/objects.get?status=${defaultStatus}`),
      axios.get('/api/objects.firstlevel.get'),
      axios.get('/api/objects.get'),
    ]).then(axios.spread((areas, categories, objects, objectsFirstLevel, searchObjects) => ({
      areas: areas.data,
      categories: categories.data.data,
      objects: objects.data,
      objectsFirstLvl: objectsFirstLevel.data,
      searchObjects: searchObjects.data
    })))
  }

  initSlider() {
    var currentSlide = 1
    var maxSlides = $('div.img-list').find('img').length

    $('div.img-list').find('img').each(function(i) {
      var left = i * 100
      $(this).css('left', left + '%')
    })

    $('div.buttons').find('div.prev').on('click', () => {
      if (currentSlide === 1) {
        currentSlide = maxSlides
        $('div.img-list').css('transform', 'translateX(-' + ((maxSlides - 1) * 100) + '%)')
      } else {
        currentSlide -= 2
        $('div.img-list').css('transform', 'translateX(-' + (currentSlide * 100) + '%)')
        currentSlide += 1
      }
    })

    $('div.buttons').find('div.next').on('click', () => {
      if (currentSlide > maxSlides - 1) {
        currentSlide = 1
        $('div.img-list').css('transform', 'translateX(-0%)')
        return
      } else {
        $('div.img-list').css('transform', 'translateX(-' + (currentSlide * 100) + '%)')
      }

      currentSlide += 1
    })

    var t = this

    $('div.pay-block').find('div').each(function(i) {
      $(this).on('click', function() {
        $('div.pay-block').find('div').each(function() {
          $(this).removeClass('active')
        })

        $(this).addClass('active')
        t.props.dispatch(paySet('currentIndex', i))
        document.getElementById('pay').classList.remove('disabled')
      })
    })
  }

  addRealtyPlacemark(object) {
    var placemark = new ymaps.Placemark([object.lat, object.lng], {
      iconContent: ReactDOM.renderToString(<div className="svg-marker" dangerouslySetInnerHTML={{__html: house}}></div>)
    }, {
      iconImageHref: "/static/images/marker-active.png",
      iconImageSize: [40, 60],
    })

    placemark.events.add('click', () => {
      var isOpen = this.props.map.balloon.isOpen()
      if (isOpen) {
        this.props.map.balloon.close()
        return
      }

      placemark.options.set('iconImageHref', "/static/images/marker-chosen.png")
      var balloon = BalloonObjEvent(object, this.props.map, this.props.pay, this.props._auth)

      balloon.events.add('close', () => {
        placemark.options.set('iconImageHref', "/static/images/marker-active.png")
      })
      this.initSlider.bind(this)()

      if (document.getElementById('route-setter')) {
        document.getElementById('route-setter').addEventListener('click', () => {
          this.props.dispatch(toggleCategories(false))
          balloon.close()
          this.props.dispatch(directionSet('lat', object.lat))
          this.props.dispatch(directionSet('lng', object.lng))
          this.props.dispatch(directionSet('to', object.street))
          this.props.dispatch(directionSet('opened', true))
        })
      }

      if (document.getElementById('pay')) {
        document.getElementById('pay').addEventListener('click', () => {
          if (!this.props._auth.isAuth) {
            this.props.dispatch(authSet('popupRegistrationOpen', true))
            this.props.dispatch(authSet('registrationPay', true))
          } else {
            axios.get(`/api/generate.liqpay.data?amount=${this.props.pay.days[this.props.pay.currentIndex].amount}&token=${localStorage.getItem('token')}`).then((resp) => {
              var data = resp.data.data
              var signature = resp.data.signature

              this.props.dispatch(paySet('data', data))
              this.props.dispatch(paySet('signature', signature))
              this.props.dispatch(paySet('showPopup', true))
            })
          }
        })
      }
    })

    return placemark
  }

  refreshRealtyObjects() {
    this.state.objects.forEach((object) => {
      var placemark = this.addRealtyPlacemark(object)
      this.props.map.geoObjects.add(placemark)
      this.objectsPlacemarks.push(placemark)
    })
  }

  processCategories() {
    var areas = cloneDeep(this.state.areas)
    areas = areas.map((area) => {
      area.title += ' район'
      return area
    })
    areas.unshift({id: -1, title: "Все районы"})

    this.setState({areas, categories: this.state.categories.filter((categorie) => categorie.objects.length > 0)}, () => {
      if (this.state.fullCategories === undefined)
        this.setState({fullCategories: this.state.categories})

      var objectActive = []
      var objectFirstLvlActive = []
      var listActive = []

      this.state.categories.forEach((el) => {
        listActive = []
        el.objects.forEach(() => listActive.push(false))
        objectActive.push({active: false, obj: listActive})
      })

      this.state.objectsFirstLvl.forEach((el) => objectFirstLvlActive.push(false))
      this.setState({objectActive, objectFirstLvlActive})
    })
  }

  processData() {
    this.refreshRealtyObjects()
    this.processCategories.bind(this)()
  }

  fetchData() {
    var initialState = getInitialState()
    if (initialState) {
      var { areas, categories, objects, objectsFirstLvl, searchObjects } = initialState
      this.setState({areas, categories, objects, objectsFirstLvl, searchObjects}, () => this.processData.bind(this)())
    } else {
      Category.initialData().then(initialData => {
        var { areas, categories, objects, objectsFirstLvl, searchObjects } = initialData
        this.setState({areas, categories, objects, objectsFirstLvl, searchObjects}, () => this.processData.bind(this)())
      })
    }
  }

  componentWillMount() {
    var initialData = this.props.initialData
    if (initialData) {
      var { areas, categories, objects, objectsFirstLvl, searchObjects } = initialData
      this.setState({areas, categories, objects, objectsFirstLvl, searchObjects}, () => this.processData.bind(this)())
    }
  }

  componentDidMount() {
    this.fetchData()
  }

  updateObjects() {
    if (this.state.rent && this.state.sale) {
      this.filter.status = undefined
    } else {
      this.state.rent ? this.filter.status = 'Сдается' : null
      this.state.sale ? this.filter.status = 'Продается' : null

      if (!this.state.rent && !this.state.sale)
        this.filter.status = undefined
    }

    var objectToUrl = (object) => (
      Object.keys(object).map((key) => [key, object[key]])
      .filter((object) => object[1] !== undefined)
      .map((object) => object[0] + '=' + object[1])
      .join('&')
    )

    this.objectsPlacemarks.forEach((placemark) => this.props.map.geoObjects.remove(placemark))
    axios.get('/api/objects.get?' + objectToUrl(this.filter)).then((objects) => this.setState({objects: objects.data}, () => {
      this.refreshRealtyObjects()
    }))
  }

  rentHouse(rent) {
    this.setState({rent}, () => this.updateObjects.bind(this)())
  }

  saleHouse(sale) {
    this.setState({sale}, () => this.updateObjects.bind(this)())
  }

  areaUpdate(option) {
    if (option.id !== -1) {
      var categories = cloneDeep(this.state.fullCategories)
      categories = categories.map((cat) => {
        cat.objects = cat.objects.map((object) => {
          object.objects = object.objects.filter((obj) => {
            if (!isNull(obj.area)) {
              return obj.area.id === option.id
            } else {
              return false
            }
          })

          return object
        })

        return cat
      })

      categories = categories.map((cat) => {
        cat.objects = cat.objects.filter((object) => {
          return object.objects.length > 0
        })

        return cat
      }).filter((categorie) => categorie.objects.length > 0)

      this.setState({categories})
      this.filter.area_id = option.id
      this.updateObjects.bind(this)()
    } else {
      this.setState({categories: this.state.fullCategories})
      this.filter.area_id = undefined
      this.updateObjects.bind(this)()
    }
  }

  radioChangeState(i) {
    if (this.content[i].value === 'more') {
      this.filter.more = true
      this.filter.room_count = 4
    } else {
      this.filter.more = undefined
      this.filter.room_count = this.content[i].value
    }

    this.updateObjects.bind(this)()
  }

  checkObjectsAndDeleteMarkers() {
    var existsObject = (idx) => this.state.selectObjects.filter((obj) => obj.id === idx).length > 0

    this.placemarks.forEach((marker, idx) => {
      if (!existsObject(idx)) {
        marker !== undefined && marker.forEach((placemark) => this.props.map.geoObjects.remove(placemark))
        this.placemarks[idx] = undefined
      }
    })
  }

  refreshDeepObjects() {
    var objects = this.state.selectObjects
  }

  addObjectPlacemark(object, icon) {
    var placemark = new ymaps.Placemark([object.lat, object.lng], {
      iconContent: ReactDOM.renderToString(<div className="svg-marker" dangerouslySetInnerHTML={{__html: icon || house}}></div>)
    }, {
      iconImageHref: "/static/images/marker-active.png",
      iconImageSize: [40, 60],
    })

    this.props.map.geoObjects.add(placemark)

    placemark.events.add('click', () => {
      if(this.props.map.balloon.isOpen()) {
        this.props.map.balloon.close()
        return
      }

      placemark.options.set('iconImageHref', "/static/images/marker-chosen.png")
      var balloon = BalloonEvent(object, this.props.map)
      balloon.events.add('close', () => {
        placemark.options.set('iconImageHref', "/static/images/marker-active.png")
      })

      document.getElementById('route-setter').addEventListener('click', () => {
        this.props.dispatch(toggleCategories(false))
        balloon.close()
        this.props.dispatch(directionSet('lat', object.lat))
        this.props.dispatch(directionSet('lng', object.lng))
        this.props.dispatch(directionSet('to', object.title))
        this.props.dispatch(directionSet('opened', true))
      })
    })

    return placemark
  }

  refreshObjects(pic) {
    var objects = this.state.selectObjects
    var objectsList = []
    var placemarks = []

    objects.forEach((el) => {
      placemarks = []

      if (el.objects.length > 0) {
        el.objects.forEach((object) => {
          objectsList.push(object)

          if (this.placemarks[el.id] === undefined) {
            var placemark = this.addObjectPlacemark.bind(this)(object, pic || undefined)
            placemarks.push(placemark)
          }
        })

        if (this.placemarks[el.id] === undefined)
          this.placemarks[el.id] = placemarks
      }
    })

    this.checkObjectsAndDeleteMarkers()
    this.props.dispatch(objectsSet('objectsList', objectsList))
  }

  search(e) {
    var foundObjects = []

    if (e.target.value.length === 0) {
      this.setState({foundObjects})
      return
    }

    this.state.searchObjects.forEach((object) => {
      if (object.street.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1
          || object.description.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1) {
        foundObjects.push({
          id: object.id,
          title: object.street,
          type: 'flat',
          placemark: undefined,
          pic: house,
          object: object,
          active: false})
      }
    })

    this.state.categories.forEach((cat) => {
      cat.objects.forEach((catObject) => {
        catObject.objects.forEach((object) => {
          if (object.title.toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1) {
            foundObjects.push({
              id: object.id,
              title: object.title,
              type: 'object',
              pic: cat.pic.left_panel_icon,
              object: object,
              active: false})
          }
        })
      })
    })

    this.setState({foundObjects})
  }

  render() {
    return (
      <div>
        <div className={classNames("left-sidebar", {"hidden": !this.props.categoriesOpened})}>
          <div className="left-slidebar__header" onClick={() => this.props.dispatch(toggleCategories())}>
            <h2>Категории</h2>

            <span className="left-sidebar__close"></span>
          </div>

          <Scrollbars
            renderThumbVertical={({style, props}) => (
              <div style={assign({}, style, {width: 10, transition: 'height 350ms ease', backgroundColor: '#448aff'})} {...props} />
            )}
            autoHide={true}
            style={{height: 'calc(100% - 50px)'}}
            hideTracksWhenNotNeeded={false}
            universal>

            <div className="left_siderbar__content">
              <div className="left_siderbar__search">
                <input placeholder="Живой поиск..." onChange={this.search.bind(this)} />
                <div className="left_siderbar__search__btn"></div>
              </div>

              <ul className="search-result">
                {this.state.foundObjects.length > 0 && this.state.foundObjects.map((object, i) => {
                  return (
                    <li key={i} className={classNames({'active': this.state.foundObjects[i].active})} onClick={() => {
                        var foundObjects = this.state.foundObjects
                        foundObjects[i].active = !foundObjects[i].active
                        var foundPlacemarks = this.state.foundPlacemarks
                        if (foundObjects[i].type === 'object' && this.state.foundPlacemarks[object.id] === undefined) {
                          var placemark = this.addObjectPlacemark.bind(this)(foundObjects[i].object, this.state.foundObjects[i].pic)
                          foundPlacemarks[object.id] = placemark
                          this.props.map.setCenter([foundObjects[i].object.lat, foundObjects[i].object.lng], 17, {checkZoomRange: true})
                          this.setState({foundPlacemarks})
                        } else if (this.state.foundPlacemarks[object.id] !== undefined) {
                          this.props.map.geoObjects.remove(this.state.foundPlacemarks[object.id])
                          foundPlacemarks[object.id] = undefined
                          this.setState({foundPlacemarks})
                        }

                        if (foundObjects[i].type === 'flat' && foundObjects[i].placemark === undefined) {
                          this.addRealtyPlacemark(foundObjects[i].object)
                          var placemark = this.addRealtyPlacemark(foundObjects[i].object)
                          this.props.map.geoObjects.add(placemark)
                          foundObjects[i].placemark = placemark
                          this.props.map.setCenter([foundObjects[i].object.lat, foundObjects[i].object.lng], 17, {checkZoomRange: true})
                        } else if (foundObjects[i].placemark !== undefined) {
                          this.props.map.geoObjects.remove(foundObjects[i].placemark)
                          foundObjects[i].placemark = undefined
                        }

                        this.setState({foundObjects})
                      }}>
                        <object>{object.title}</object>
                        <span dangerouslySetInnerHTML={{__html: isNull(this.state.foundObjects[i].pic) ? '' : this.state.foundObjects[i].pic}}></span>
                      </li>
                  )
                })}
              </ul>

              <div className="left_sidebar__hr"></div>

              {this.state.areas && <Dropdown
                onUpdate={this.areaUpdate.bind(this)}
                options={this.state.areas}
                label="Выберите район" />}

              <div className="left_siderbar__houses"><span></span><span>Квартиры</span></div>

              <div className="left_siderbar__checkbox_group">
                <Checkbox label="Сдается" onChangeState={this.rentHouse.bind(this)} />
                <Checkbox label="Продается" onChangeState={this.saleHouse.bind(this)} active={true} />
              </div>

              <div className="left_siderbar__label">Количество комнат</div>

              <RadioButtonGroup content={this.content} onChangeState={this.radioChangeState.bind(this)} />

              <div className="left_sidebar__hr"></div>

              {this.state.categories && this.state.objectActive && this.state.categories.map((categorie, i) => {
                return (<ul className={classNames('object-list', {'active': this.state.objectActive[i].active})} key={i}>
                  <span onClick={() => {
                    var objectActiveCopy = this.state.objectActive
                    objectActiveCopy[i].active = !objectActiveCopy[i].active
                    this.setState({objectActive: objectActiveCopy})
                  }}><object dangerouslySetInnerHTML={{__html: arrow}}></object>{categorie.title}</span>

                {categorie.objects.map((el, idx) => {
                    return <li className={classNames({'active': this.state.objectActive[i].obj[idx]})} key={idx}
                    onClick={() => {
                      var objectActiveCopy = this.state.objectActive
                      objectActiveCopy[i].obj[idx] = !objectActiveCopy[i].obj[idx]
                      var selectObjectsCopy = this.state.selectObjects
                      selectObjectsCopy.push(this.state.categories[i].objects[idx])
                      if (!this.state.objectActive[i].obj[idx]) {
                        selectObjectsCopy = selectObjectsCopy.filter((el) =>
                          el.id !== selectObjectsCopy.filter((el) => el.id === this.state.categories[i].objects[idx].id)[0].id)
                      }
                      this.setState({objectActive: objectActiveCopy, selectObjects: selectObjectsCopy}, () => this.refreshObjects.bind(this)(categorie.pic.left_panel_icon))
                    }}>
                      <span dangerouslySetInnerHTML={{__html: isNull(categorie.pic) ? '' : categorie.pic.left_panel_icon}}></span>
                      {el.title}
                      <object>{el.objects.length}</object>
                    </li>
                  })}
                </ul>)
              })}

              <ul className={classNames('object-list', 'active')}>
                {this.state.objectsFirstLvl && this.state.objectFirstLvlActive && this.state.objectsFirstLvl.map((object, i) => {
                  return object.objects.length > 0 && (<li className={classNames({'active': this.state.objectFirstLvlActive[i]})} key={i} onClick={() => {
                      var objectFirstLvlActiveCopy = this.state.objectFirstLvlActive
                      objectFirstLvlActiveCopy[i] = !objectFirstLvlActiveCopy[i]
                      var selectObjectsCopy = this.state.selectObjects
                      selectObjectsCopy.push(object)
                      if (!this.state.objectFirstLvlActive[i]) {
                        selectObjectsCopy = selectObjectsCopy.filter((object) => object.id !== this.state.objectsFirstLvl[i].id)
                      }
                      selectObjectsCopy = selectObjectsCopy.filter((thing, index, self) => self.findIndex((t) => t.id === thing.id) === index)
                      this.setState({objectFirstLvlActive: objectFirstLvlActiveCopy, selectObjects: selectObjectsCopy}, () => this.refreshObjects.bind(this)(object.icon))
                    }}>
                    <span dangerouslySetInnerHTML={{__html: object.icon}}></span>
                    {object.title}
                    <object>{object.objects.length}</object>
                  </li>)
                })}
              </ul>
            </div>
          </Scrollbars>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    map: state.map.mapObject,
    objects: state.objects.objectsList,
    categoriesOpened: state.categories.opened,
    pay: state.pay,
    _auth: state.auth,
  })
)(Category)
