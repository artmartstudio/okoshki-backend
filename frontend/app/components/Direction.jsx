import React from 'react'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'
import ReactDOM from 'react-dom/server'
import { capitalize } from 'lodash/string'
import { assign } from 'lodash/object'

import { directionSet } from 'actions/direction'
import { toggleCategories } from 'actions/categories'
import { Scrollbars } from 'react-custom-scrollbars'

import 'styles/direction'

class Direction extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      from: '',
      to: '',
      route: undefined,
      routePoints: undefined,
    }
  }

  buildRoute() {
    var route = this.props.direction.reverse
      ? [[this.props.direction.lat, this.props.direction.lng], this.state.to]
      : [this.state.from, [this.props.direction.lat, this.props.direction.lng]]

    this.state.route && this.props.map.geoObjects.remove(this.state.route)

    ymaps.route(route, {
      mapStateAutoApply: true,
      avoidTrafficJams: true,
    }).then(function(route) {
      var points = route.getWayPoints()
      if (this.props.direction.reverse) {
        points.get(0).options.set('visible', false)
        points.get(1).options.set('iconImageHref', "/static/images/marker-chosen.png")
        points.get(1).options.set('iconImageSize', [40, 60])
        points.get(1).properties.set('iconContent', ReactDOM.renderToString(<div className="svg-marker-text">B</div>))
      } else {
        points.get(1).options.set('visible', false)
        points.get(0).options.set('iconImageHref', "/static/images/marker-chosen.png")
        points.get(0).options.set('iconImageSize', [40, 60])
        points.get(0).properties.set('iconContent', ReactDOM.renderToString(<div className="svg-marker-text">A</div>))
      }

      var routePoints = [], way, segments
      for (var i = 0; i < route.getPaths().getLength(); i++) {
        way = route.getPaths().get(i)
        segments = way.getSegments()
        for (var j = 0; j < segments.length; j++) {
          var street = segments[j].getStreet()
          var action = segments[j].getHumanAction()
          var className = segments[j].getAction()

          routePoints.push({
            route: capitalize(action) + (street ? ', ' + street : '') + ', проезжаем ' + segments[j].getLength() + ' м.',
            className: className,
          })
        }
      }

      // this.refs.scrollbar.refs.container.style.height = 'calc(100vh - 240px)'
      // this.refs.direction.style.height = 'calc(100vh - 200px)'
      this.props.map.geoObjects.add(route)
      this.setState({route, routePoints})
      this.props.dispatch(toggleCategories(false))
    }.bind(this))
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.direction.to !== nextProps.direction.to)
      this.setState({to: nextProps.direction.to})
  }

  render() {
    return (
      <div className={classNames("direction", {"hidden": !this.props.direction.opened})} ref="direction">
        <div className="direction-header">
          <h2>Настройка маршрута</h2>
          <span className="direction-header__close" onClick={() => {
              this.props.dispatch(directionSet('opened', false))
              this.props.dispatch(toggleCategories(true))
            }}></span>
        </div>

        <div className="direction-content">
          <div className="direction-content-flex">
            <span className="direction-map-btn"></span>
            <input placeholder="Введите адрес" value={this.state.from} ref="dest" onChange={(e) => {
                this.setState({from: e.target.value})
              }} />
            <span className="direction-map-reverse" onClick={() => {
                var from = this.state.from
                var to = this.state.to
                this.setState({to: from, from: to})
                this.props.dispatch(directionSet('reverse', !this.props.direction.reverse))
              }}></span>
            <input value={this.state.to} ref="where" onChange={(e) => {
                this.setState({to: e.target.value})
              }} />
            <span className="direction-map-car"></span>
            <button className="direction-button" onClick={this.buildRoute.bind(this)}>Проложить маршрут</button>
          </div>

          <div className="diretion-points">
            {this.state.routePoints && this.state.routePoints.map((point, i) => {
              return <div key={i} className={point.className}>{point.route}</div>
            })}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    direction: state.direction,
    map: state.map.mapObject,
  })
)(Direction)
