import React from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { ymapSetObject } from 'actions/map'
import { paySet } from 'actions/pay'
import axios from 'axios'

import Direction from 'components/Direction'
import Popup from 'components/Popup'

import 'styles/main.sass'

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    axios.get('/api/currency.settings.get').then((resp) => {
      this.props.dispatch(paySet('days', resp.data))
    })

    axios.get('/api/settings.get').then((resp) => {
      this.props.dispatch(paySet('isPay', resp.data.pay))
    })

    window.addEventListener('load', () => {
      var map = new ymaps.Map('map', {
        center: [50.4501, 30.5234],
        zoom: 10,
        behaviors: ['drag', 'scrollZoom', 'multiTouch'],
      })

      var trafficControl = new ymaps.control.TrafficControl({shown: false})
      map.controls.add(trafficControl, {top: 45, right: 310})

      this.props.dispatch(ymapSetObject('mapObject', map))
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pay.showPopup && nextProps.pay.data && nextProps.pay.signature) {
      this.forceUpdate(() => this.refs.liqpayform.submit())
    }
  }

  addZoom() {
    this.props.map.mapObject.setZoom(this.props.map.mapObject.getZoom() + 1)
  }

  subZoom() {
    this.props.map.mapObject.setZoom(this.props.map.mapObject.getZoom() - 1)
  }

  render() {
    return (
      <div>
        <Helmet title='Okoshki.net' />

        <div className="map" id="map"></div>

        {this.props.map.mapObject && (
          <div className="plus-minus-wrapper">
            <div onClick={this.addZoom.bind(this)}>+</div>
            <div onClick={this.subZoom.bind(this)}>-</div>
          </div>
        )}

        <Direction />

        {this.props.children}

        {this.props.pay.showPopup && <div className="hidden">
          <form method="POST" acceptCharset="utf-8" action="https://www.liqpay.com/api/3/checkout" ref="liqpayform">
            <input type="hidden" name="data" value={this.props.pay.data} />
            <input type="hidden" name="signature" value={this.props.pay.signature} />
          </form>
        </div>}
      </div>
    )
  }
}

export default connect(
  state => ({
    map: state.map,
    pay: state.pay
  })
)(App)
