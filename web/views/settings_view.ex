defmodule Okoshkinet.SettingsView do
  use Okoshkinet.Web, :view

  def render("index.json", %{settings: settings}) do
    %{data: render_many(settings, Okoshkinet.SettingsView, "settings.json")}
  end

  def render("show.json", %{settings: settings}) do
    %{data: render_one(settings, Okoshkinet.SettingsView, "settings.json")}
  end

  def render("settings.json", %{settings: settings}) do
    %{id: settings.id,
      pay: settings.pay,
      about_pic: settings.about_pic.file_name,
      site_name: settings.site_name}
  end
end
