defmodule Okoshkinet.CategoriesObjectsView do
  use Okoshkinet.Web, :view

  @root_dir File.cwd!
  @uploads_path Path.join(~w(#{@root_dir} priv static uploads))

  def render("index.json", %{categories_objects: categories_objects}) do
    render_many(categories_objects, Okoshkinet.CategoriesObjectsView, "categories_objects.json")
  end

  # def render("show.json", %{categories_objects: categories_objects}) do
  #   %{data: render_one(categories_objects, Okoshkinet.CategoriesObjectsView, "categories_objects.json")}
  # end

  defp readFile(path) do
    cond do
      File.exists?(path) ->
        {:ok, file} = :file.open(path, [:read, :binary])
        %{size: size} = File.stat! path
        {:ok, body} = :file.read(file, size)
        body
      true -> nil
    end
  end

  def render("categories_objects.json", %{categories_objects: categories_objects}) do
    filename = if categories_objects.icon != nil, do: categories_objects.icon.file_name, else: ""
    icon = readFile("#{@uploads_path}/categorie_pics/#{filename}")

    %{id: categories_objects.id,
      title: categories_objects.title,
      icon: icon,
      pin_active: categories_objects.pin_active,
      # pic: render_one(categories_objects.categories_objects_pics, Okoshkinet.CategoriesView, "categories_objects_pics.json", as: :role),
      objects: render_many(categories_objects.subsidiary_objects, Okoshkinet.CategoriesView, "subsidiary_objects.json", as: :role)}
  end
end
