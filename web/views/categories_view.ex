defmodule Okoshkinet.CategoriesView do
  use Okoshkinet.Web, :view

  @root_dir File.cwd!
  @uploads_path Path.join(~w(#{@root_dir} priv static uploads))

  def render("index.json", %{categories: categories}) do
    %{data: render_many(categories, Okoshkinet.CategoriesView, "categories.json")}
  end

  def render("show.json", %{categories: categories}) do
    %{data: render_one(categories, Okoshkinet.CategoriesView, "categories.json")}
  end

  defp readFile(path) do
    cond do
      File.exists?(path) ->
        {:ok, file} = :file.open(path, [:read, :binary])
        %{size: size} = File.stat! path
        {:ok, body} = :file.read(file, size)
        body
      true -> nil
    end
  end

  def render("categories_objects_pics.json", %{role: role}) do
    filename = if role.left_panel_icon != nil, do: role.left_panel_icon.file_name, else: ""
    left_panel_icon_svg = readFile("#{@uploads_path}/categorie_pics/#{filename}")

    %{
      id: role.id,
      # active: role.active,
      # disable: role.disable,
      left_panel_icon: left_panel_icon_svg
    }
  end

  def render("subsidiary_objects_phones.json", %{role: role}) do
    %{phone: role.phone}
  end

  def render("subsidiary_objects_work_time.json", %{role: role}) do
    %{
      days_range: role.days_range,
      time: role.time
    }
  end

  def render("subsidiary_objects.json", %{role: role}) do
    newrole = role |> Okoshkinet.Repo.preload([:subsidiary_objects_phones, :subsidiary_objects_work_time, :areas])

    %{
      id: role.id,
      title: role.title,
      address: role.address,
      photo: role.photo,
      lat: role.lat,
      lng: role.lng,
      description: role.description,
      link: role.link,
      phones: render_many(newrole.subsidiary_objects_phones, Okoshkinet.CategoriesView, "subsidiary_objects_phones.json", as: :role),
      area: render_one(newrole.areas, Okoshkinet.AreasView, "show.json", as: :areas),
      work_time: render_many(newrole.subsidiary_objects_work_time, Okoshkinet.CategoriesView, "subsidiary_objects_work_time.json", as: :role)
    }
  end

  def render("object.json", %{role: role}) do
    newrole = role |> Okoshkinet.Repo.preload([:subsidiary_objects])

    %{
      id: newrole.id,
      title: newrole.title,
      objects: render_many(newrole.subsidiary_objects, Okoshkinet.CategoriesView, "subsidiary_objects.json", as: :role),
    }
  end

  def render("categories.json", %{categories: categories}) do
    %{id: categories.id,
      title: categories.title,
      pic: render_one(categories.categories_objects_pics, Okoshkinet.CategoriesView, "categories_objects_pics.json", as: :role),
      objects: render_many(categories.categories_objects, Okoshkinet.CategoriesView, "object.json", as: :role)}
  end
end
