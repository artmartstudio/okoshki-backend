defmodule Okoshkinet.CurrencySettingsView do
  use Okoshkinet.Web, :view

  def render("index.json", %{currency_settings: currency_settings}) do
    render_many(currency_settings, Okoshkinet.CurrencySettingsView, "currency_settings.json")
  end

  def render("show.json", %{currency_settings: currency_settings}) do
    %{data: render_one(currency_settings, Okoshkinet.CurrencySettingsView, "currency_settings.json")}
  end

  def render("currency_settings.json", %{currency_settings: currency_settings}) do
    %{day: currency_settings.day,
      amount: currency_settings.price}
  end
end
