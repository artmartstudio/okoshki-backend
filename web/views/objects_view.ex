defmodule Okoshkinet.ObjectsView do
  use Okoshkinet.Web, :view

  def render("index.json", %{objects: objects}) do
    render_many(objects, Okoshkinet.ObjectsView, "objects.json")
  end

  def render("show.json", %{objects: objects}) do
    %{data: render_one(objects, Okoshkinet.ObjectsView, "objects.json")}
  end

  def render("areas.json", %{role: role}) do
    %{id: role.id, title: role.title}
  end

  def render("objects.json", %{objects: objects_old}) do
    objects = objects_old |> Okoshkinet.Repo.preload([:areas, :object_contacts])

    %{id: objects.id,
      area: render_one(objects.areas, Okoshkinet.ObjectsView, "areas.json", as: :role),
      street: objects.street,
      lat: objects.lat,
      lng: objects.lng,
      room_count: objects.room_count,
      status: objects.status,
      price: objects.price,
      in_dollars: objects.in_dollars,
      description: objects.description,
      phones: render_many(objects.object_contacts, Okoshkinet.CategoriesView, "subsidiary_objects_phones.json", as: :role),
      photo1: objects.photo1,
      photo2: objects.photo2,
      photo3: objects.photo3}
  end
end
