defmodule Okoshkinet.AreasView do
  use Okoshkinet.Web, :view

  def render("index.json", %{areas: areas}) do
    render_many(areas, Okoshkinet.AreasView, "areas.json")
  end

  def render("show.json", %{areas: areas}) do
    render_one(areas, Okoshkinet.AreasView, "areas.json")
  end

  def render("areas.json", %{areas: areas}) do
    %{id: areas.id,
      title: areas.title}
  end
end
