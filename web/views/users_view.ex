defmodule Okoshkinet.UsersView do
  use Okoshkinet.Web, :view

  def render("show.json", %{user: user}) do
    %{id: user.id, email: user.email, expiration_at: user.expiration_at, password: user.password}
  end
end
