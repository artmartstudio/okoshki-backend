defmodule Okoshkinet.StringGenerator do
  @chars "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890" |> String.split("")

  def random_string(length) do
    Enum.reduce((1..length), [], fn (_i, acc) ->
      [Enum.random(@chars) | acc]
    end) |> Enum.join("")
  end
end
