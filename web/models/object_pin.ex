defmodule Okoshkinet.ObjectPin do
  use Okoshkinet.Web, :model

  schema "object_pin" do
    field :active, Okoshkinet.ObjectPhoto.Type
    field :disable, Okoshkinet.ObjectPhoto.Type
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:active, :disable])
    |> validate_required([:active, :disable])
  end
end
