defmodule Okoshkinet.CurrencySettings do
  use Okoshkinet.Web, :model

  schema "currency_settings" do
    field :day, :integer
    field :price, :integer
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:day, :price])
    |> validate_required([:day, :price])
  end
end
