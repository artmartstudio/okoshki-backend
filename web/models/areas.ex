defmodule Okoshkinet.Areas do
  use Okoshkinet.Web, :model

  @derive {Poison.Encoder, except: [:__meta__]}

  schema "areas" do
    field :title, :string
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
  end
end
