defmodule Okoshkinet.SubsidiaryObjectsPhones do
  use Okoshkinet.Web, :model

  schema "subsidiary_objects_phones" do
    field :phone, :string
    has_many :group_subsidiary_objects_phones, Okoshkinet.GroupSubsidiaryObjectsPhones, on_delete: :delete_all
    has_many :subsidiary_objects, through: [:group_subsidiary_objects_phones, :subsidiary_objects]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:phone])
    |> validate_required([])
  end
end
