defmodule Okoshkinet.GroupSubsidiaryObjectsWorkTime do
  use Okoshkinet.Web, :model

  schema "group_subsidiary_objects_work_time" do
    belongs_to :subsidiary_objects, Okoshkinet.SubsidiaryObjects
    belongs_to :subsidiary_objects_work_time, Okoshkinet.SubsidiaryObjectsWorkTime
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:subsidiary_objects_id, :subsidiary_objects_work_time_id])
    |> validate_required([])
  end
end
