defmodule Okoshkinet.SubsidiaryObjects do
  use Okoshkinet.Web, :model

  schema "subsidiary_objects" do
    field :title, :string
    field :address, :string
    field :lat, :string
    field :lng, :string
    field :photo, Okoshkinet.ObjectPhoto.Type
    field :link, :string
    field :description, :string
    belongs_to :categories_objects, Okoshkinet.CategoriesObjects, foreign_key: :categories_objects_id
    belongs_to :areas, Okoshkinet.Areas, foreign_key: :area_id

    has_many :group_subsidiary_objects_phones, Okoshkinet.GroupSubsidiaryObjectsPhones, on_delete: :delete_all
    has_many :subsidiary_objects_phones, through: [:group_subsidiary_objects_phones, :subsidiary_objects_phones]

    has_many :group_subsidiary_objects_work_time, Okoshkinet.GroupSubsidiaryObjectsWorkTime, on_delete: :delete_all
    has_many :subsidiary_objects_work_time, through: [:group_subsidiary_objects_work_time, :subsidiary_objects_work_time]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :address, :lat, :lng, :photo, :link, :description, :categories_objects_id, :area_id])
    |> validate_required([:title, :address, :lat, :lng])
  end
end
