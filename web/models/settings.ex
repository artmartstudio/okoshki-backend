defmodule Okoshkinet.Settings do
  use Okoshkinet.Web, :model

  schema "settings" do
    field :pay, :boolean, default: false
    field :site_name, :string
    field :about_pic, Okoshkinet.ObjectPhoto.Type
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:pay, :site_name, :about_pic])
    |> validate_required([:pay, :site_name, :about_pic])
  end
end
