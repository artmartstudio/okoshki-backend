defmodule Okoshkinet.SubsidiaryObjectsWorkTime do
  use Okoshkinet.Web, :model

  schema "subsidiary_objects_work_time" do
    field :days_range, :string
    field :time, :string
    has_many :group_subsidiary_objects_work_time, Okoshkinet.GroupSubsidiaryObjectsWorkTime, on_delete: :delete_all
    has_many :subsidiary_objects, through: [:group_subsidiary_objects_work_time, :subsidiary_objects]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:days_range, :time])
    |> validate_required([])
  end
end
