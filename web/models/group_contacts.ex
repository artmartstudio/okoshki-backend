defmodule Okoshkinet.GroupContacts do
  use Okoshkinet.Web, :model

  schema "group_contacts" do
    belongs_to :objects, Okoshkinet.Objects
    belongs_to :object_contacts, Okoshkinet.ObjectContacts
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:objects_id, :object_contacts_id])
    |> assoc_constraint(:objects)
    |> assoc_constraint(:object_contacts)
  end
end
