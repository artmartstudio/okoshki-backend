defmodule Okoshkinet.CategoriesObjects do
  use Okoshkinet.Web, :model

  schema "categories_objects" do
    field :title, :string
    field :icon, Okoshkinet.CategoriePic.Type
    field :pin_active, Okoshkinet.CategoriePic.Type
    has_many :subsidiary_objects, Okoshkinet.SubsidiaryObjects
    belongs_to :categories, Okoshkinet.Categories, foreign_key: :categories_id
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :categories_id, :icon, :pin_active])
    |> validate_required([:title])
  end

  def filterNil(struct) do
    from object in struct,
    where: is_nil(object.categories_id)
  end
end
