defmodule Okoshkinet.ObjectContacts do
  use Okoshkinet.Web, :model

  @derive {Poison.Encoder, except: [:__meta__, :groups_contacts, :objects]}

  schema "object_contacts" do
    field :phone, :string
    has_many :groups_contacts, Okoshkinet.GroupContacts, on_delete: :delete_all
    has_many :objects, through: [:groups_contacts, :objects]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:phone])
    |> validate_required([:phone])
    |> validate_length(:phone, min: 7, max: 18)
    |> validate_format(:phone, ~r/\([0-9]{3}\)(\s|-)[0-9]{3}-[0-9]{2}-[0-9]{2}/)
  end
end
