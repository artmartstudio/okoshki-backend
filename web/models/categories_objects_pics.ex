defmodule Okoshkinet.CategoriesObjectsPics do
  use Okoshkinet.Web, :model

  schema "categories_objects_pics" do
    # field :active, Okoshkinet.CategoriePic.Type
    # field :disable, Okoshkinet.CategoriePic.Type
    field :left_panel_icon, Okoshkinet.CategoriePic.Type
    belongs_to :categories, Okoshkinet.Categories, foreign_key: :categories_id
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:left_panel_icon, :categories_id])
    |> validate_required([:left_panel_icon]) #:active, :disable, :left_panel_icon, :categories_objects_id
  end
end
