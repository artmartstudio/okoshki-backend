defmodule Okoshkinet.Categories do
  use Okoshkinet.Web, :model

  schema "categories" do
    field :title, :string
    has_many :categories_objects, Okoshkinet.CategoriesObjects
    has_one :categories_objects_pics, Okoshkinet.CategoriesObjectsPics
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
  end
end
