defmodule Okoshkinet.Objects do
  use Okoshkinet.Web, :model
  use Arc.Ecto.Schema

  @derive {Poison.Encoder, except: [:__meta__, :users, :groups_contacts]}

  schema "objects" do
    field :street, :string
    field :lat, :string
    field :lng, :string
    field :room_count, :integer
    field :status, :string
    field :price, :integer
    field :in_dollars, :boolean, default: false
    field :description, :string
    field :photo1, Okoshkinet.ObjectPhoto.Type
    field :photo2, Okoshkinet.ObjectPhoto.Type
    field :photo3, Okoshkinet.ObjectPhoto.Type
    field :checked, :boolean
    belongs_to :areas, Okoshkinet.Areas, foreign_key: :area_id
    belongs_to :users, Okoshkinet.Users, foreign_key: :user_id
    has_many :groups_contacts, Okoshkinet.GroupContacts, on_delete: :delete_all
    has_many :object_contacts, through: [:groups_contacts, :object_contacts], on_delete: :delete_all

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:street, :lat, :lng, :room_count, :status,
                     :price, :in_dollars, :description, :area_id,
                     :user_id, :photo1, :photo2, :photo3, :checked])
    |> cast_attachments(params, [:photo1, :photo2, :photo3])
    |> validate_required([:street, :lat, :lng, :room_count, :status, :description])
  end

  def filterMore(struct, room_count) do
    from object in struct,
    where: object.room_count >= ^room_count
  end

  def checkedOnly(struct) do
    from object in struct,
    where: object.checked == true
  end
end
