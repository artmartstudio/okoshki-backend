defmodule Okoshkinet.Users do
  use Okoshkinet.Web, :model

  alias Comeonin.Bcrypt

  @cast ~w(expiration_at token password password_hash)a
  @required_fields ~w(email)a
  @derive {Poison.Encoder, except: [:__meta__, :password]}

  schema "users" do
    field :email, :string
    field :password, :string
    field :password_hash, :string
    field :token, :string
    field :expiration_at, Timex.Ecto.DateTime
    has_many :objects, Okoshkinet.Objects, foreign_key: :user_id

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @cast ++ @required_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:email)
    |> validate_length(:email, min: 6)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 5, max: 100)
    |> hashp
  end

  defp hashp(changeset) do
    case changeset do
      %{valid?: true, changes: %{password: _password, expiration_at: _expiration_at}} ->
        put_change(changeset, :password_hash, Bcrypt.hashpwsalt(_password))
      _ ->
        changeset
    end
  end

  def check_password(model, password) do
    Bcrypt.checkpw(password, model.password_hash)
  end
end
