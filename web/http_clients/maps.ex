defmodule Okoshkinet.MapsApi do
  use HTTPoison.Base

  def process_url(street) do
    "https://geocode-maps.yandex.ru/1.x/?format=json&kind=house&geocode=#{street}"
  end

  def process_response_body(body) do
    body
    |> Poison.decode!
    |> Map.get("response")
    |> Map.get("GeoObjectCollection")
    |> Map.get("featureMember")
    |> Enum.at(0)
    |> Map.get("GeoObject")
    |> Map.get("Point")
    |> Map.get("pos")
    |> String.split(" ")
  end
end
