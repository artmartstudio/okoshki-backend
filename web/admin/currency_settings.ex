defmodule Okoshkinet.ExAdmin.CurrencySettings do
  use ExAdmin.Register

  register_resource Okoshkinet.CurrencySettings do
    menu label: "Настройки оплаты"
    filter false
    batch_actions false
    action_items except: [:new, :destroy, :delete]

    index do
      column :day
      column :price
    end
  end
end
