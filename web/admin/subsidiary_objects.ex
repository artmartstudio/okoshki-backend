defmodule Okoshkinet.ExAdmin.SubsidiaryObjects do
  use ExAdmin.Register
  alias Okoshkinet.{Repo, CategoriesObjects, Areas}

  @contacts "Контакты"
  @phone_number "Номер телефона"
  @work_time "Время работы"

  register_resource Okoshkinet.SubsidiaryObjects do
    menu label: "Дочерние объекты", priority: 3

    filter false
    batch_actions false

    index do
      column :id
      column :title
      column :categories_objects
      column :link
      actions
    end

    query do
       %{
         all: [preload: [:subsidiary_objects_phones, :categories_objects, :subsidiary_objects_work_time]],
       }
     end

    show subsidiary_objects do
      attributes_table do
        row :id
        row :title
        row :address
        row :lat
        row :lng
        row :link
        row :description
        row :categories_objects
        row :photo, fn h ->
          Okoshkinet.ExAdmin.Objects.prev_photo h, :photo, Okoshkinet.ObjectPhoto
        end
      end

      panel @contacts do
        table_for subsidiary_objects.subsidiary_objects_phones do
          column :phone, label: @phone_number
        end
      end

      panel @work_time do
        table_for subsidiary_objects.subsidiary_objects_work_time do
          column :days_range
          column :time
        end
      end
    end

    form subsidiary_objects do
      inputs do
        input subsidiary_objects, :title
        input subsidiary_objects, :address
        input subsidiary_objects, :lat
        input subsidiary_objects, :lng
        input subsidiary_objects, :photo
        input subsidiary_objects, :link
        input subsidiary_objects, :description, type: :text
        input subsidiary_objects, :categories_objects, collection: Repo.all(CategoriesObjects), fields: [:title]
        input subsidiary_objects, :areas, collection: Repo.all(Areas), fields: [:title]
      end

      inputs @contacts do
        has_many subsidiary_objects, :subsidiary_objects_phones, fn(p) ->
          input p, :phone
        end
      end

      inputs @work_time do
        has_many subsidiary_objects, :subsidiary_objects_work_time, fn(p) ->
          input p, :days_range
          input p, :time
        end
      end
    end
  end
end
