defmodule Okoshkinet.ExAdmin.Objects do
  use ExAdmin.Register
  alias Okoshkinet.{Repo, Areas}

  @object_status ~w(Сдается Продается)
  @in_dollars "В долларах"
  @contacts "Контакты"
  @phone_number "Номер телефона"
  @slider_photos "Фотографии для слайдера"

  def prev_photo(row, coll, obj) do
    map_coll = Map.get(row, coll)
    cond do
      map_coll != nil ->
        img src: obj.asset_url({map_coll, row})

      true ->
        img src: ""
    end
  end

  register_resource Okoshkinet.Objects do
    menu label: "Объекты (квартиры)", priority: 5
    filter false
    batch_actions false

    query do
       %{
         all: [preload: [:object_contacts, :areas]],
       }
     end

    index do
      column :id
      column :street
      column :room_count
      column :price
      column :inserted_at
      column :updated_at
      column :checked
      actions
    end

    show objects do
      attributes_table do
        row :id
        row :street
        row :room_count
        row :price
        row :lat
        row :lng
        row :status
        row :in_dollars
        row :description
        row :areas
        row :photo1, fn h ->
          prev_photo h, :photo1, Okoshkinet.ObjectPhoto
        end
        row :photo2, fn h ->
          prev_photo h, :photo2, Okoshkinet.ObjectPhoto
        end
        row :photo3, fn h ->
          prev_photo h, :photo3, Okoshkinet.ObjectPhoto
        end
      end

      panel @contacts do
        table_for objects.object_contacts do
          column :phone, label: @phone_number
        end
      end
    end

    form objects do
      inputs do
        input objects, :street
        input objects, :lat
        input objects, :lng
        input objects, :room_count
        input objects, :price
        input objects, :in_dollars, label: @in_dollars
        input objects, :description, type: :text
        input objects, :areas, collection: Repo.all(Areas), fields: [:title]
        input objects, :status, collection: @object_status
        input objects, :photo1
        input objects, :photo2
        input objects, :photo3
      end

      inputs @contacts do
        has_many objects, :object_contacts, fn(p) ->
          input p, :phone
        end
      end
    end
  end
end
