defmodule Okoshkinet.ExAdmin.Areas do
  use ExAdmin.Register

  register_resource Okoshkinet.Areas do
    menu label: "Районы Киева", priority: 4
    filter false
    batch_actions false    
  end
end
