defmodule Okoshkinet.ExAdmin.Settings do
  use ExAdmin.Register

  register_resource Okoshkinet.Settings do
    menu label: "Настройки сайта"

    filter false
    action_items except: [:new, :destroy, :delete]
    batch_actions false

    index do
      column :pay
      column :site_name
    end
  end
end
