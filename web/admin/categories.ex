defmodule Okoshkinet.ExAdmin.Categories do
  use ExAdmin.Register

  register_resource Okoshkinet.Categories do
    menu label: "Категории", priority: 1

    filter false
    batch_actions false
  end
end
