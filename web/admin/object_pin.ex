defmodule Okoshkinet.ExAdmin.ObjectPin do
  use ExAdmin.Register

  register_resource Okoshkinet.ObjectPin do
    menu label: "Pin`ы для квартир", priority: 6

    filter false
    batch_actions false
    action_items except: [:new, :destroy, :delete]

    index do
      column :active, fn h ->
        Okoshkinet.ExAdmin.Objects.prev_photo h, :active, Okoshkinet.ObjectPhoto
      end
      column :disable, fn h ->
        Okoshkinet.ExAdmin.Objects.prev_photo h, :disable, Okoshkinet.ObjectPhoto
      end
      actions
    end

    show object_pin do
      attributes_table do
        row :id
        row :active, fn h ->
          Okoshkinet.ExAdmin.Objects.prev_photo h, :active, Okoshkinet.ObjectPhoto
        end
        row :disable, fn h ->
          Okoshkinet.ExAdmin.Objects.prev_photo h, :disable, Okoshkinet.ObjectPhoto
        end
      end
    end
  end
end
