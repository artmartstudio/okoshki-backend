defmodule Okoshkinet.ExAdmin.CategoriesObjects do
  use ExAdmin.Register
  alias Okoshkinet.{Repo, Categories}

  defp category_name(categories_objects) do
    (categories_objects.categories || %{title: ""}).title
  end

  register_resource Okoshkinet.CategoriesObjects do
    menu label: "Объекты категорий", priority: 2
    filter false
    batch_actions false

    index do
      column :id
      column :title
      column :categories, &category_name/1
      actions
    end

    show _categories_objects do
      attributes_table do
        row :title
        row :categories, &category_name/1
      end
    end

    form categories_objects do
      inputs do
        input categories_objects, :title
        input categories_objects, :icon
        input categories_objects, :pin_active
        input categories_objects, :categories, collection: Repo.all(Categories), fields: [:title]
      end
    end
  end
end
