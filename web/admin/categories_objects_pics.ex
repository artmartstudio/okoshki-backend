defmodule Okoshkinet.ExAdmin.CategoriesObjectsPics do
  use ExAdmin.Register
  alias Okoshkinet.{Repo, Categories}

  defp category_name(categories_objects) do
    (categories_objects.categories || %{title: ""}).title
  end

  register_resource Okoshkinet.CategoriesObjectsPics do
    menu label: "Pin`ы для объектов категорий", priority: 7
    filter false
    batch_actions false

    index do
      column :id
      column :categories, &category_name/1
      column :left_panel_icon, fn h ->
        Okoshkinet.ExAdmin.Objects.prev_photo h, :left_panel_icon, Okoshkinet.CategoriePic
      end
      actions
    end

    form categories_objects do
      inputs do
        # input categories_objects, :active
        # input categories_objects, :disable
        input categories_objects, :left_panel_icon
        input categories_objects, :categories, collection: Repo.all(Categories), fields: [:title]
      end
    end
  end
end
