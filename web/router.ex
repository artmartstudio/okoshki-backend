defmodule Okoshkinet.Router do
  use Okoshkinet.Web, :router
  use ExAdmin.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, [origin: "*"]
    plug :accepts, ["json"]
  end

  scope "/admin", ExAdmin do
    pipe_through [:browser]
    admin_routes
  end

  scope "/", Okoshkinet do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", Okoshkinet do
   pipe_through :api

  #  get "/content.set", ApiController, :content_set
   get "/object.update/:id", ApiController, :update_object
   get "/object.delete/:id", ApiController, :delete_object
   post "/object.add", ApiController, :add_object
   post "/object.update/:id", ApiController, :update_rows_object
   options "/object.update/:id", ApiController, :options

  #  get "/objects.update", ApiController, :update_objects

   get "/kiev.areas.get", AreasController, :index
   get "/categories.get", CategoriesController, :index
   get "/objects.get", ObjectsController, :index
   get "/objects.firstlevel.get", ApiController, :firstlevel_get
   get "/generate.liqpay.data", ApiController, :liqpay_data
   get "/user.subscribe.confirm", ApiController, :liqpay_confirm
   get "/currency.settings.get", CurrencySettingsController, :index
   get "/settings.get", ApiController, :settings_get

   post "/email.send", ApiController, :email_send
   options "/email.send", ApiController, :options

   post "/user.get.email", AuthController, :user_get_email
   options "/user.get.email", AuthController, :options

   post "/user.get", AuthController, :user_get
   options "/user.get", AuthController, :options

   post "/auth", AuthController, :auth
   options "/auth", AuthController, :options

   post "/isauth", AuthController, :isauth
   options "/isauth", AuthController, :options

   post "/logout", AuthController, :logout
   options "/logout", AuthController, :options

   post "/signup", AuthController, :signup
   options "/signup", AuthController, :options
  end
end
