defmodule Okoshkinet.ApiController do
  use Okoshkinet.Web, :controller
  use Bamboo.Phoenix, view: Okoshkinet.EmailView
  use Timex

  alias Okoshkinet.{CategoriesObjects, Mailer, Email, Users, Settings, CurrencySettings,
  SubsidiaryObjects, SubsidiaryObjectsWorkTime, GroupSubsidiaryObjectsWorkTime,
  GroupSubsidiaryObjectsPhones, SubsidiaryObjectsPhones, Objects, ObjectContacts,
  GroupContacts}

  # def check_and_parse(string) do
  #   cond do
  #     string != nil && String.length(string) > 0 ->
  #       String.replace(string, "\"", "")
  #
  #     true ->
  #       string
  #   end
  # end
  #
  # def content_set(conn, args) do
  #   data = File.read! "priv/golos.csv"
  #   split = Enum.drop(String.split(data, "\n"), 1)
  #
  #   Enum.map(split, fn(value) ->
  #     split_value = String.split value, ";"
  #
  #     name = check_and_parse(Enum.at(split_value, 0))
  #     link = check_and_parse(Enum.at(split_value, 1))
  #     address = check_and_parse(Enum.at(split_value, 2))
  #     phones = check_and_parse(Enum.at(split_value, 3))
  #     lng = check_and_parse(Enum.at(split_value, 4))
  #     lat = check_and_parse(Enum.at(split_value, 5))
  #     work_time = check_and_parse(Enum.at(split_value, 6))
  #     description = check_and_parse(Enum.at(split_value, 7))
  #
  #     cond do
  #       String.length(name) > 0 ->
  #         phones_split = String.split(phones, ",")
  #         # work_time_split = String.split(work_time, ",")
  #
  #         changeset = %SubsidiaryObjects{}
  #         |> SubsidiaryObjects.changeset(%{title: name,
  #                                          address: address,
  #                                          lat: lat,
  #                                          lng: lng,
  #                                          link: link,
  #                                          description: description})
  #
  #         work_time_changeset = %SubsidiaryObjectsWorkTime{}
  #         |> SubsidiaryObjectsWorkTime.changeset(%{time: work_time})
  #
  #         sub = Repo.insert!(changeset)
  #         worktime = Repo.insert!(work_time_changeset)
  #
  #         gwork_time_changeset = %GroupSubsidiaryObjectsWorkTime{}
  #         |> GroupSubsidiaryObjectsWorkTime.changeset(%{subsidiary_objects_id: sub.id, subsidiary_objects_work_time_id: worktime.id})
  #
  #         Repo.insert!(gwork_time_changeset)
  #
  #         Enum.map(phones_split, fn(phone) ->
  #           cond do
  #             phone != nil && String.length(phone) > 0 ->
  #               phones_changeset = %SubsidiaryObjectsPhones{}
  #               |> SubsidiaryObjectsPhones.changeset(%{phone: phone})
  #               subphones = Repo.insert!(phones_changeset)
  #
  #               gphone_changeset = %GroupSubsidiaryObjectsPhones{}
  #               |> GroupSubsidiaryObjectsPhones.changeset(%{subsidiary_objects_id: sub.id, subsidiary_objects_phones_id: subphones.id})
  #               Repo.insert!(gphone_changeset)
  #
  #             true ->
  #           end
  #         end)
  #
  #         # Enum.map(work_time_split, fn(value_wt) ->
  #         #   value_wt_split = String.split(value_wt, ":")
  #         #   days_range = Enum.at(value_wt_split, 0) |> String.trim
  #         #   time = Enum.at(value_wt_split, 1)
  #         #
  #         #   work_time_changeset = %SubsidiaryObjectsWorkTime{}
  #         #   |> SubsidiaryObjectsWorkTime.changeset(%{time: time, days_range: days_range})
  #         #   subwt = Repo.insert!(work_time_changeset)
  #         #
  #         #   gwork_time_changeset = %GroupSubsidiaryObjectsWorkTime{}
  #         #   |> GroupSubsidiaryObjectsWorkTime.changeset(%{subsidiary_objects_id: sub.id, subsidiary_objects_work_time_id: subwt.id})
  #         #
  #         #   Repo.insert!(gwork_time_changeset)
  #         # end)
  #
  #       true ->
  #     end
  #   end)
  #
  #   conn
  #   |> send_resp(200, "")
  # end

  def update_objects do
    objects = Repo.all(Objects) |> Repo.preload(:users)
    Enum.each(objects, fn(object) ->
      iat_erl = object.inserted_at |> Ecto.DateTime.to_erl
      expDays = Calendar.DateTime.from_erl!(iat_erl, "Europe/Kiev")
      |> Calendar.Date.diff(Calendar.DateTime.now!("Europe/Kiev"))

      if expDays == -25 do
        if object.users != nil do
          Email.send(object.users.email, "Ваш объект будет удалён - okoshki.net", "Объект по адресу #{object.street} будет удалён из сайта через 5 дней. Вы можете обновить его в личном кабинете!") |> Mailer.deliver_later
        end
      end

      if expDays <= -30 do
        Repo.delete!(object)
      end
    end)
  end

  def update_object(conn, %{"id" => id}) do
    object = Repo.get_by(Objects, id: id)
    |> Repo.preload([:areas, :object_contacts])

    changeset = object
    |> Ecto.Changeset.change(inserted_at: Ecto.DateTime.utc)
    |> Repo.update!

    json conn, %{"status" => "success"}
  end

  def delete_object(conn, %{"id" => id}) do
    object = Repo.get_by(Objects, id: id)
    Repo.delete!(object)
    json conn, %{"status" => "success"}
  end

  def add_object(conn, opts \\ %{}) do
    object = Map.get(opts, "object") |> Poison.decode!

    file_list = Enum.reduce([0, 1, 2], %{}, fn x, acc ->
      file = Map.get(opts, "file#{x}")
      Map.put(acc, "photo#{x+1}", file)
    end) |> Enum.filter(fn {_, v} -> v != nil end) |> Enum.into(%{}) |> AtomicMap.convert(safe: true)

    latlng_split = Okoshkinet.MapsApi.get!(Map.get(object, "street")).body
    latlng = %{lat: Enum.at(latlng_split, 1), lng: Enum.at(latlng_split, 0)}

    changeset = Objects.changeset(%Objects{}, object
    |> Map.drop(["phones"])
    |> AtomicMap.convert(safe: true)
    |> Map.put(:lat, latlng.lat)
    |> Map.put(:lng, latlng.lng)
    |> Map.merge(file_list))

    object_repo = Repo.insert!(changeset)

    Enum.each(object |> Map.get("phones"), fn(phone) ->
      oc_changeset = ObjectContacts.changeset(%ObjectContacts{}, %{phone: phone})
      object_contacts = Repo.insert!(oc_changeset)

      gc_changeset = GroupContacts.changeset(%GroupContacts{}, %{})
      |> Ecto.Changeset.put_assoc(:objects, object_repo)
      |> Ecto.Changeset.put_assoc(:object_contacts, object_contacts)

      Repo.insert!(gc_changeset)
    end)

    json conn, %{"status" => "success"}
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end

  def update_rows_object(conn, opts \\ %{}) do
    id = Map.get(opts, "id")
    object = Map.get(opts, "object") |> Poison.decode!

    file_list = Enum.reduce([0, 1, 2], %{}, fn x, acc ->
      file = Map.get(opts, "file#{x}")
      if file != nil do
        extension = Path.extname(file.filename)
        unique_filename = random_string(12) <> extension
        path = "priv/static/uploads/object_photos/original_#{unique_filename}#{extension}"
        File.cp(file.path, path)
        Map.put(acc, "photo#{x+1}", %{file_name: unique_filename, updated_at: Ecto.DateTime.utc})
      else
        Map.put(acc, "photo#{x+1}", nil)
      end

    end) |> Enum.filter(fn {_, v} -> v != nil end) |> Enum.into(%{}) |> AtomicMap.convert(safe: true)

    latlng_split = Okoshkinet.MapsApi.get!(Map.get(object, "street")).body
    latlng = %{lat: Enum.at(latlng_split, 1), lng: Enum.at(latlng_split, 0)}

    new_object = object
    |> Map.drop(["object_contacts", "areas", "phones", "photo1", "photo2", "photo3", "updated_at", "inserted_at"])
    |> AtomicMap.convert(safe: true)
    |> Map.put(:lat, latlng.lat)
    |> Map.put(:lng, latlng.lng)
    |> Map.merge(file_list)

    object_repo = Repo.get_by(Objects, id: id)
    |> Repo.preload([:object_contacts])

    Enum.each(object_repo.object_contacts, &(Repo.delete!(&1)))

    changeset = object_repo
    |> Ecto.Changeset.change(new_object)

    Enum.each(object |> Map.get("phones"), fn(phone) ->
      oc_changeset = ObjectContacts.changeset(%ObjectContacts{}, %{phone: phone})
      object_contacts = Repo.insert!(oc_changeset)

      gc_changeset = GroupContacts.changeset(%GroupContacts{}, %{})
      |> Ecto.Changeset.put_assoc(:objects, object_repo)
      |> Ecto.Changeset.put_assoc(:object_contacts, object_contacts)

      Repo.insert!(gc_changeset)
    end)

    Repo.update!(changeset)

    json conn, %{"status" => "success"}
  end

  def settings_get(conn, args) do
    settings = Repo.one!(Settings)
    json conn, %{pay: settings.pay, site_name: settings.site_name, about_pic: settings.about_pic.file_name}
  end

  def liqpay_confirm(conn, %{"amount" => amount, "token" => token}) do
    user = Repo.get_by(Users, token: token)
    cs = Repo.get_by(CurrencySettings, price: amount)

    expiration_at = cond do
      user.expiration_at == nil ->
        Timex.shift(Timex.now("EET"), days: cs.day)

      true ->
        Timex.shift(user.expiration_at, days: cs.day)
    end

    changeset = Users.changeset(user, %{expiration_at: expiration_at})
    Repo.update!(changeset)
    redirect conn, to: "/"
  end

  def liqpay_data(conn, %{"amount" => amount, "token" => token}) do
    url = Atom.to_string(conn.scheme) <> "://" <> (Enum.into(conn.req_headers, %{}) |> Map.get("host"))
    public_key = "i22913745782"
    private_key = "vYa56qNuPbPJmiHgA7wcOCWC7fxN0x2lvvaKYxxO"

    json_data = %{
      action: "pay",
      amount: amount,
      currency: "UAH",
      version: "3",
      public_key: public_key,
      description: "Подписка на просмотр контактов объектов",
      result_url: "#{url}/api/user.subscribe.confirm?token=#{token}&amount=#{amount}"#,
      #sandbox: "1" # debug
    }

    data = Poison.encode!(json_data)
    |> Base.encode64

    signature = :crypto.hash(:sha, "#{private_key}#{data}#{private_key}")
    |> Base.encode64

    conn
    |> json(%{data: data, signature: signature})
  end

  def firstlevel_get(conn, _params) do
    objects = CategoriesObjects
    |> CategoriesObjects.filterNil
    |> Repo.all
    |> Repo.preload([:subsidiary_objects])

    render(conn, Okoshkinet.CategoriesObjectsView, "index.json", categories_objects: objects)
  end

  def email_send(conn, %{"to" => to, "subject" => subject, "body" => body}) do
    Email.send(to, subject, body) |> Mailer.deliver_later

    conn
    |> send_resp(200, "")
  end
end
