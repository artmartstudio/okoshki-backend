defmodule Okoshkinet.SettingsController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.Settings

  def index(conn, _params) do
    settings = Repo.all(Settings)
    render(conn, "index.json", settings: settings)
  end

  def create(conn, %{"settings" => settings_params}) do
    changeset = Settings.changeset(%Settings{}, settings_params)

    case Repo.insert(changeset) do
      {:ok, settings} ->
        conn
        |> put_status(:created)
        |> render("show.json", settings: settings)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    settings = Repo.get!(Settings, id)
    render(conn, "show.json", settings: settings)
  end

  def update(conn, %{"id" => id, "settings" => settings_params}) do
    settings = Repo.get!(Settings, id)
    changeset = Settings.changeset(settings, settings_params)

    case Repo.update(changeset) do
      {:ok, settings} ->
        render(conn, "show.json", settings: settings)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    settings = Repo.get!(Settings, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(settings)

    send_resp(conn, :no_content, "")
  end
end
