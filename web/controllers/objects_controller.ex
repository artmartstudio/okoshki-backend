defmodule Okoshkinet.ObjectsController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.Objects

  def index(conn, params) do
    rows = if Map.get(params, "more", nil) != nil,
    do: [:status, :area_id],
    else: [:room_count, :status, :area_id]

    filters = Ecto.Changeset.cast(%Objects{}, params, [], rows)
    |> Map.fetch!(:changes)
    |> Map.to_list

    objects = cond do
      Map.get(params, "more", nil) != nil ->
        Objects
        |> Objects.checkedOnly()
        |> where(^filters)
        |> Objects.filterMore(String.to_integer(Map.get(params, "room_count")))
        |> Repo.all
        |> Repo.preload([:areas, :object_contacts])

      true ->
        Objects
        |> Objects.checkedOnly()
        |> where(^filters)
        |> Repo.all
        |> Repo.preload([:areas, :object_contacts])
    end

    # json conn, objects
    render(conn, "index.json", objects: objects)
  end
end
