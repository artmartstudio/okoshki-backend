defmodule Okoshkinet.AuthController do
  use Okoshkinet.Web, :controller
  alias Okoshkinet.Users

  def user_get_email(conn, %{"email" => email}) do
    user = Okoshkinet.Repo.get_by(Users, email: email)

    conn
    |> render(Okoshkinet.UsersView, "show.json", user: user)
  end

  def user_get(conn, %{"token" => token}) do
    user = Okoshkinet.Repo.get_by(Users, token: token)
    |> Repo.preload([objects: :areas, objects: :object_contacts])

    json conn, user
  end

  def auth(conn, %{"email" => email, "password" => password}) do
    user = Repo.one(from u in Users,
            where: u.email == ^email,
            select: u)

    cond do
      user != nil and Users.check_password(user, password) ->
        token = String.downcase(Okoshkinet.StringGenerator.random_string(64))
        Repo.update!(Users.changeset(user, %{token: token, password: password}))

        conn
        |> json(%{status: "success", token: token})

      true ->
        conn
        |> json(%{status: "error"})
    end
  end

  def signup(conn, %{"email" => email, "password" => password}) do
    token = String.downcase(Okoshkinet.StringGenerator.random_string(64))
    Repo.insert(Users.changeset(%Users{}, %{email: email, password: password, token: token}))

    conn
    |> json(%{status: "success", token: token})
  end

  def isauth(conn, %{"token" => token}) do
    user = Okoshkinet.Repo.get_by(Users, token: token)

    cond do
      user != nil and user.token != nil ->
        conn
        |> json(%{status: "authenticated", token: user.token})

      true ->
        conn
        |> json(%{status: "not authenticated", token: nil})
    end
  end

  def logout(conn, %{"token" => token}) do
    user = Okoshkinet.Repo.get_by(Users, token: token)
    Repo.update!(Users.changeset(user, %{token: nil}))

    conn
    |> json(%{status: "success"})
  end
end
