defmodule Okoshkinet.CategoriesObjectsController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.CategoriesObjects

  def index(conn, _params) do
    categories_objects = Repo.all(CategoriesObjects)
    render(conn, "index.json", categories_objects: categories_objects)
  end

  def create(conn, %{"categories_objects" => categories_objects_params}) do
    changeset = CategoriesObjects.changeset(%CategoriesObjects{}, categories_objects_params)

    case Repo.insert(changeset) do
      {:ok, categories_objects} ->
        conn
        |> put_status(:created)
        |> render("show.json", categories_objects: categories_objects)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    categories_objects = Repo.get!(CategoriesObjects, id)
    render(conn, "show.json", categories_objects: categories_objects)
  end

  def update(conn, %{"id" => id, "categories_objects" => categories_objects_params}) do
    categories_objects = Repo.get!(CategoriesObjects, id)
    changeset = CategoriesObjects.changeset(categories_objects, categories_objects_params)

    case Repo.update(changeset) do
      {:ok, categories_objects} ->
        render(conn, "show.json", categories_objects: categories_objects)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    categories_objects = Repo.get!(CategoriesObjects, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(categories_objects)

    send_resp(conn, :no_content, "")
  end
end
