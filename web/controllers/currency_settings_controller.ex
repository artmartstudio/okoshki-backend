defmodule Okoshkinet.CurrencySettingsController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.CurrencySettings

  def index(conn, _params) do
    currency_settings = Repo.all(CurrencySettings)
    render(conn, "index.json", currency_settings: currency_settings)
  end

  def create(conn, %{"currency_settings" => currency_settings_params}) do
    changeset = CurrencySettings.changeset(%CurrencySettings{}, currency_settings_params)

    case Repo.insert(changeset) do
      {:ok, currency_settings} ->
        conn
        |> put_status(:created)
        |> render("show.json", currency_settings: currency_settings)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    currency_settings = Repo.get!(CurrencySettings, id)
    render(conn, "show.json", currency_settings: currency_settings)
  end

  def update(conn, %{"id" => id, "currency_settings" => currency_settings_params}) do
    currency_settings = Repo.get!(CurrencySettings, id)
    changeset = CurrencySettings.changeset(currency_settings, currency_settings_params)

    case Repo.update(changeset) do
      {:ok, currency_settings} ->
        render(conn, "show.json", currency_settings: currency_settings)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Okoshkinet.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    currency_settings = Repo.get!(CurrencySettings, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(currency_settings)

    send_resp(conn, :no_content, "")
  end
end
