defmodule Okoshkinet.AreasController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.Areas

  def index(conn, _params) do
    areas = Repo.all(Areas)
    render(conn, "index.json", areas: areas)
  end

  def show(conn, %{"id" => id}) do
    areas = Repo.get!(Areas, id)
    render(conn, "show.json", areas: areas)
  end
end
