defmodule Okoshkinet.PageController do
  use Okoshkinet.Web, :controller
  alias Okoshkinet.Settings

  def index(conn, _params) do
    title = Repo.one!(Settings)

    conn
    |> assign(:title, title.site_name)
    |> render("index.html")
  end
end
