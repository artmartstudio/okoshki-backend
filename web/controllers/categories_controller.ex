defmodule Okoshkinet.CategoriesController do
  use Okoshkinet.Web, :controller

  alias Okoshkinet.Categories

  def index(conn, _params) do
    categories = Repo.all(Categories) |> Repo.preload([:categories_objects, :categories_objects_pics])
    render(conn, "index.json", categories: categories)
  end

  def show(conn, %{"id" => id}) do
    categories = Repo.get!(Categories, id)
    render(conn, "show.json", categories: categories)
  end
end
