defmodule Okoshkinet.ObjectPhoto do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Okoshkinet.BaseUploader

  @versions [:original]

  # To add a thumbnail version:
  # @versions [:original, :thumb]

  # Whitelist file extensions:
  def validate({file, _}) do
    ~w(.jpg .jpeg .png .svg)
    |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  # def transform(:thumb, _) do
  #   {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250 -format png", :png}
  # end

  # Override the persisted filenames:
  # def filename(version, _) do
  #   version
  # end

  def filename(version,  {file, scope}) do
    "#{version}_#{file.file_name}"
  end

  # Override the storage directory:
  def storage_dir(_version, {_file, _scope}) do
    "#{static_path}uploads/object_photos"
  end

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version, scope) do
  #   "/static/uploads/object_photos/original_#{version}.png"
  # end
end
