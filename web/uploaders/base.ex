defmodule Okoshkinet.BaseUploader do
  defmacro __using__(_) do
    quote do
      def __storage, do: Arc.Storage.Local

      def static_path, do: "priv/static/"

      def asset_url(file) when is_tuple(file) do
        file
        |> url
        |> String.replace(static_path, "/")
      end
    end
  end
end
